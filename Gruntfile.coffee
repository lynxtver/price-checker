module.exports = (grunt)->
	grunt.initConfig(
		pkg: grunt.file.readJSON "package.json"
		gruntconfig: do->
			cfg = 
				out: "out"

		paths:
			src: "src"
			client: "<%=paths.src%>/client"
			server: "<%=paths.src%>/server/"
			target: "<%=gruntconfig.out%>"
			public: "<%=paths.target%>/public"

		clean:
			server:
				src: [
					"<%=gruntconfig.out%>/*.js"
					"<%=gruntconfig.out%>/conf"
					"<%=gruntconfig.out%>/lib"
					"<%=gruntconfig.out%>/fonts"
				]
			client:
				src: [
					"<%=paths.public%>/css"
					"<%=paths.public%>/fonts"
					"<%=paths.public%>/img/clock"
					"<%=paths.public%>/img/flags"
					"<%=paths.public%>/img/lang"
					"<%=paths.public%>/img/*.*"
					"<%=paths.public%>/js"
					"<%=paths.public%>/locales"
					"<%=paths.public%>/templates"
					"<%=paths.public%>/*.*"
				]
			options:
				force: true

		coffee:
			server:
				expand: true
				cwd: "<%=paths.server%>"
				src: ["**/*.coffee"]
				dest: "<%=paths.target%>"
				ext: ".js"

			client:
				expand: true
				cwd: "<%=paths.client%>/coffee/"
				src: ["**/*.coffee"]
				dest: "<%=paths.public%>/js/"
				ext: ".js"

		less:
			debug:
				files: "<%=paths.public%>/css/common.css": ["<%=paths.client%>/less/common.less"]
			release:
				options:
					yuicompress: true
				files: "<%=paths.public%>/css/common.css": ["<%=paths.client%>/less/common.less"]

		jade:
			client:
				options:
					pretty: true
				files: [
					cwd: "<%=paths.client%>/templates"
					src: "**/*.jade"
					dest: "<%=paths.public%>/templates"
					expand: true
					ext: ".html"
				]
			server:
				options:
					pretty: true
				files: [
					cwd: "<%=paths.client%>/views"
					src: "**/*.jade"
					dest: "<%=paths.public%>"
					expand: true
					ext: ".html"
				]

		copy:
			assets:files: [
				expand: true
				cwd: "<%=paths.client%>/assets/"
				src: "**"
				dest: "<%=paths.public%>/"
			]
			libjs:
				files:[
					expand: true
					flatten: true
					cwd: "bower_components/" 
					src: [
						"requirejs/require.js"
						"requirejs-text/text.js"
						"backbone/backbone.js"
						"backbone.marionette/lib/core/backbone.marionette.js"
						"backbone.wreqr/lib/backbone.wreqr.js"
						"backbone.babysitter/lib/backbone.babysitter.js"
						"backbone.paginator/lib/backbone.paginator.js"
						"underscore/underscore.js"
						"moment/moment.js"
						"jquery/dist/jquery.js"
						"requirejs-domready/domReady.js"
						"log4javascript/log4javascript.js"
						"bootstrap/dist/js/bootstrap.js"
						"underscore.string/lib/underscore.string.js"
						"select2/select2.js"
						"backbone-validation/dist/backbone-validation-amd.js"
						"backbone.stickit/backbone.stickit.js"
						"i18next/i18next.amd.js"
						"require-i18next/i18next.js"
						"require-i18next/i18next-builder.js"
						"datatables/media/js/jquery.dataTables.js"
						"datatables-bootstrap3/BS3/assets/js/datatables.js"
						"dropzone/downloads/dropzone-amd-module.js"
						"numeral/numeral.js"
						"bootstrap-datepicker/js/bootstrap-datepicker.js"
						"bootstrap-datepicker/js/locales/bootstrap-datepicker.ru.js"
						"smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"
						"smalot-bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.ru.js"
						"async/lib/async.js"
						"iCheck/icheck.js"
						"jquery-form/jquery.form.js"
						"jasny-bootstrap/js/fileinput.js"
						"highcharts/highcharts.js"
					] 
					dest: "<%=paths.public%>/js/lib"
				,
					expand: true
					flatten: true
					cwd: "bower_components/"
					src: "numeral/languages/ru.js"
					dest: "<%=paths.public%>/js/lib"
					rename: (destBase, destPath)->
						"#{destBase}/numeral.ru.js"
				,
					expand: true
					flatten: true
					cwd: "bower_components/"
					src: "moment/locale/ru.js"
					dest: "<%=paths.public%>/js/lib"
					rename: (destBase, destPath)->
						"#{destBase}/moment.ru.js"
				]
			select2:files:[
				expand: true
				flatten: true
				cwd: "bower_components/select2/"
				src: [
					"select2.png"
					"select2-spinner.gif"
				]
				dest: "<%=paths.public%>/css"
			]
			icheck:files:[
				expand: true
				flatten: true
				cwd: "bower_components/iCheck/skins/flat"
				src: "*.png"
				dest: "<%=paths.public%>/css"
			]
			locales:files: [
				expand: true
				cwd: "<%=paths.client%>/locales"
				src: ["**/*.json"]
				dest: "<%=paths.public%>/locales"
			]
			fonts:files:[
				expand: true
				flatten: true
				cwd: "bower_components/"
				src: [
					"bootstrap/dist/fonts/*"
					"font-awesome/fonts/*"
				] 
				dest: "<%=paths.public%>/fonts" 
			]
			server_fonts:files:[
				expand: true
				flatten: true
				cwd: "<%=paths.server%>/fonts"
				src: ["*.*"] 
				dest: "<%=paths.target%>/fonts" 
			]

		requirejs:
			client:
				options:
					name: "main"
					baseUrl: "<%=paths.public%>/js/"
					mainConfigFile: "<%=paths.public%>/js/main.js"
					out: "<%=paths.public%>/js/main.js"
					preserveLicenseComments: false
					optimize: "uglify2"
					paths:
						underscore: "empty:"
						marionette: "empty:"
						backbone: "empty:"
						"backbone.wreqr": "empty:"
						"backbone.babysitter": "empty:"
						jquery: "empty:"
						"jquery-ui": "empty:"
						domReady: "empty:"
						moment: "empty:"
						"underscore.string": "empty:"
						"backbone.stickit": "empty:"
						"backbone-validation": "empty:"
						bootstrap: "empty:"
					wrapShim: true
					generateSourceMaps: false

		watch:
			options:
				nospawn: true
			coffeeclient:
				files: ['**/*.coffee']
				tasks: ['coffee:client']
				options: 
					cwd: "<%=paths.client%>/coffee"
			coffeeserver:
				files: ['**/*.coffee']
				tasks: ['coffee:server']
				options: 
					cwd: "<%=paths.server%>"
			locales:
				files: ['**/*.json']
				tasks: ['copy:locales']
				options: 
					cwd: "<%=paths.client%>/locales"
			templates:
				files: ['**/*.jade']
				tasks: ['jade:client']
				options:
					cwd: "<%=paths.client%>/templates"
			less:
				files: ['**/*.less']
				tasks: ['less:debug']
				options:
					cwd: "<%=paths.client%>/less"
			jade:
				files: ['**/*.jade']
				tasks: ['jade:server']
				options:
					cwd: "<%=paths.client%>/views"

		nodemon: 
			options:
				cwd: "<%=paths.target%>"
				ignore: ['public/**']
			debug:
				script: "server.js"
				options:
					nodeArgs: ['--debug']

			trace:
				script: "server.js"
				options:
					nodeArgs: ['--debug-brk']
			
			release: 
				script: "server.js"

			cluster:
				script: "cluster.js"
				
		concurrent: 
			options:
				logConcurrentOutput: true			
			debug:
				tasks: ['nodemon:debug', 'watch']
			trace:
				tasks: ['nodemon:trace', 'watch']

		)
	
	grunt.registerTask 'build', (arg)->
		buildType = arguments[0] ? "debug"
		if buildType not in ['debug', 'release', 'trace']
			grunt.log.error "Wrong build type specified"
			false
		else
			grunt.log.ok "Build type: #{buildType}"

			switch buildType
				when "debug", "trace" then grunt.task.run 'clean', 'less:debug', 'coffee', 'jade', 'copy'
				when "release" then grunt.task.run 'clean', 'less:release', 'coffee', 'jade', 'copy', 'requirejs'
				else
					grunt.log.error "Wrong build type specified"
					false

	grunt.registerTask 'run', (arg)->
		buildType = arguments[0] ? "debug"
		environment = arguments[1] ? "development"

		if environment not in ['development', 'production']
			grunt.log.error "Wrong environment specified"
			false

		grunt.log.ok "Runtime environment: #{environment}"

		grunt.task.run "build:#{buildType}", if buildType is "release" then "nodemon:release" else "concurrent:#{buildType}"

	grunt.registerTask 'trace', ['run:trace']
	grunt.registerTask 'debug', ['run:debug']
	grunt.registerTask 'release', ['run:release']
	grunt.registerTask 'cluster', ['build:debug', 'nodemon:cluster']
	grunt.registerTask 'default', ['run']
	
	grunt.loadNpmTasks "grunt-contrib-requirejs"
	grunt.loadNpmTasks "grunt-contrib-clean"
	grunt.loadNpmTasks "grunt-contrib-coffee"
	grunt.loadNpmTasks "grunt-contrib-copy"
	grunt.loadNpmTasks "grunt-contrib-jade"
	grunt.loadNpmTasks "grunt-contrib-less"
	grunt.loadNpmTasks "grunt-contrib-watch"
	grunt.loadNpmTasks "grunt-concurrent"
	grunt.loadNpmTasks "grunt-contrib-cssmin"
	grunt.loadNpmTasks "grunt-nodemon"