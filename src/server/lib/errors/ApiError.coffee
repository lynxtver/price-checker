class ApiError extends Error

	constructor:(@message = "API_ERROR", @status = 400)->
		super
		@name = "ApiError"

module.exports = ApiError