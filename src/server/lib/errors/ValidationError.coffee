class ValidationError extends Error

	constructor:(@fields = {}, @message = "API_ERROR", @status = 400)->
		super
		@type = "ValidationError"

module.exports = ValidationError