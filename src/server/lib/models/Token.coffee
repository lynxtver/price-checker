module.exports = (callback = ->)->
	mongoose = require "mongoose"
	Chance = require "chance"
	path = require "path"
	ObjectId = mongoose.Schema.Types.ObjectId

	App = require path.join "..", "App"

	chance = new Chance

	Logger = App.Logger

	Logger.info "TokenModel", "Init"

	TokenSchema = new mongoose.Schema
		hash:
			type: String
			require: true
		user: 
			type: ObjectId
			ref: "User"
		timestamp:
			type: Date
			default: Date.now
			expires: "7d"
	,
		versionKey: false

	TokenSchema.statics.getToken = (hash, callback)->
		@findOneAndRemove
			hash: hash
			(err, token)->
				return callback err if err
				callback null, token?.user

	TokenSchema.statics.setToken = (id, callback)->
		token = chance.hash length: 64
		@create
			hash: token
			user: id
		, callback

	TokenSchema.statics.issueToken = (user, done)->
		App.Models.Token.setToken user.id, (err, token)->
			return done err if err
			done null, token.hash

	App.Models.Token = mongoose.model "Token", TokenSchema

	Logger.info "TokenModel", "Init complete"
	
	do callback