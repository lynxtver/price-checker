module.exports = (callback = ->)->
	mongoose = require "mongoose"
	path = require "path"
	async = require "async"
	ObjectId = mongoose.Schema.Types.ObjectId

	App = require path.join "..", "App"

	Logger = App.Logger

	Logger.info "GoodModel", "Init"

	GoodSchema = new mongoose.Schema
		name:
			type: String
			required: 'error:common.required'
		links: [
			shop:
				type: ObjectId
				ref: "Shop"
				required: 'error:common.required'
			url:
				type: String
				required: 'error:common.required'
		]
	,
		toObject: 
			virtuals: true
		toJSON: 
			virtuals: true

	App.Models.Good = mongoose.model "Good", GoodSchema

	initCallback = ->
		Logger.info "GoodModel", "Init complete"
		callback null

	do callback