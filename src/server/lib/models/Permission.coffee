module.exports = (callback = ->)->
	async = require "async"
	mongoose = require "mongoose"
	path = require "path"

	App = require path.join "..", "App"

	Logger = App.Logger

	Logger.info "PermissionModel", "Init"

	PermissionSchema = new mongoose.Schema
		name:
			type: String
			required: 'STR_REQUIRED_FIELD'
			trim: true
			index:
				unique: true
		created:
			type: Date
			default: Date.now
		modified:
			type: Date
	,
		toObject: 
			virtuals: true
		toJSON: 
			virtuals: true

	PermissionSchema.statics.getList = (callback)->
		@find callback

	App.Models.Permission = mongoose.model "Permission", PermissionSchema

	initCallback = ->
		Logger.info "PermissionModel", "Init complete"
		callback null

	permissions = [
		name: "USERS.VIEW"
	,
		name: "USERS.ADD"
	,
		name: "USERS.EDIT"
	,
		name: "USERS.REMOVE"
	,
		name: "ROLES.VIEW"
	,
		name: "ROLES.ADD"
	,
		name: "ROLES.EDIT"
	,
		name: "ROLES.REMOVE"
	,
		name: "SHOPS.VIEW"
	,
		name: "SHOPS.ADD"
	,
		name: "SHOPS.EDIT"
	,
		name: "SHOPS.REMOVE"
	,
		name: "GOODS.VIEW"
	,
		name: "GOODS.ADD"
	,
		name: "GOODS.EDIT"
	,
		name: "GOODS.REMOVE"
	,
		name: "TASKS.VIEW"
	,
		name: "PRICES.VIEW"
	,
		name: "PRICES.ADD"
	,
		name: "PRICES.EDIT"
	,
		name: "PRICES.REMOVE"
	]

	async.eachSeries permissions, (permission, cb)->
		App.Models.Permission.findOneAndUpdate
			name: permission.name
		,
			name: permission.name
		,
			upsert: true
		, cb
	, (err)->
		throw err if err
		do initCallback