module.exports = (callback = ->)->
	mongoose = require "mongoose"
	Grid = require "gridfs-stream"
	async = require "async"
	path = require "path"
	fs = require "fs"

	Grid.mongo = mongoose.mongo

	App = require path.join "..", "App"

	App.Models = {}

	files = fs.readdirSync __dirname

	App.Grid = Grid App.DB.connection.db

	async.eachSeries files, (file, cb)->
		fileName = path.basename file, ".js"
		unless path.basename(file) is path.basename(__filename)
			require(path.join __dirname, fileName)(cb)
		else
			cb null
	, (err)->
		callback null