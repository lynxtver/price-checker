module.exports = (callback = ->)->
	NestedSetPlugin = require "mongoose-nested-set"
	async = require "async"
	path = require "path"
	mongoose = require "mongoose"
	ObjectId = mongoose.Schema.Types.ObjectId

	App = require path.join "..", "App"

	Logger = App.Logger

	Logger.info "RoleModel", "Init"

	RoleSchema = new mongoose.Schema
		name:
			type: String
			required: 'error:common.required'
			match: [/^[a-z][a-z0-9._-]+$/, 'error:common.roles.format']
			trim: true
			lowercase: true
			index:
				unique: true
		created:
			type: Date
			default: Date.now
		modified:
			type: Date
		permissions: [
			type: ObjectId
			ref: "Permission"
			unique: true
		]
	,
		toObject: 
			virtuals: true
		toJSON: 
			virtuals: true

	RoleSchema.plugin NestedSetPlugin

	RoleSchema.path("name").validate (name, respond)->
		App.Models.Role.findOne {name: name}, (err, role)=>
			throw err if err
			if @isNew
				respond not role?
			else
				respond not role? or role.id is @id
	, "error:roles.exists"

	RoleSchema.path("parentId").validate (parentId, respond)->
		if parentId
			App.Models.Role.findById parentId, (err, parent)->
				respond parent?
		else
			respond true
	, "error:roles.parentNotFound"

	RoleSchema.path("parentId").validate (parentId, respond)->
		self = @
		unless parentId
			return respond true
			
		unless self.isNew
			async.waterfall [
				(cb)->
					cb null, self.id.toString() isnt parentId.toString()
				
				(result, cb)->
					unless result
						cb null, result
					else
						App.Models.Role.findById parentId, (err, parent)->
							unless parent?
								cb null, false
							else
								parent.selfAndAncestors (err, ancestors)->
									async.some ancestors, (ancestor, cb)->
										cb ancestor.id.toString() is self.id.toString()
									, (result)->
										respond not result

			], (err, result)->
				respond result
		else
			respond true
	, "error:roles.loop"

	RoleSchema.pre "save", (next)->
		if not @isNew and @isModified()
			@modified = Date.now()

		do next

	RoleSchema.methods.checkLocalPermission = (permission, callback)->
		Logger.debug "RoleModel", "#{@name}", "Search #{permission} in local permissions"
		callback null, @permissions.some (_permission)=>
			Logger.debug "RoleModel", "#{@name}", "Check local permission: #{_permission.name} == #{permission}"
			_permission.name is permission

	RoleSchema.methods.checkChildrenPermission = (permission, callback)->
		return callback null, false if @isLeaf()
		@descendants (err, descendants)->
			App.Models.Role.populate descendants, "permissions", (err, descendants)->
				return callback err if err
				async.detectSeries descendants, (descendant, cb)->
					descendant.checkLocalPermission permission, (err, allow)->
						cb allow
				, (allow)->
					callback null, allow?

	RoleSchema.methods.checkPermission = (permission, callback)->
		async.waterfall [
			(cb)=>@checkLocalPermission permission, cb
			(allow, cb)=>
				return cb null, true if allow
				@checkChildrenPermission permission, cb
			(allow, cb)->
				return cb null, true if allow
				cb null, false
		], callback

	RoleSchema.methods.getLocalPermissions = (callback)->
		@populate "permissions", (err, role)->
			return callback err if err
			callback null, role.permissions.map (permission)->
				permission.name

	RoleSchema.methods.getChildrenPermissions = (callback)->
		return callback null, [] if @isLeaf()
		@descendants (err, descendants)->
			App.Models.Role.populate descendants, "permissions", (err, descendants)->
				return callback err if err
				async.reduce descendants, [], (permissions, descendant, cb)->
					descendant.getLocalPermissions (err, localPermissions)->
						return cb err if err
						cb null, permissions.concat localPermissions
				, (err, permissions)->
					callback null, permissions

	RoleSchema.methods.getPermissions = (callback)->
		async.waterfall [
			(cb)=>@getLocalPermissions cb
			(localPermissions, cb)=>
				@getChildrenPermissions (err, childrenPermissions)->
					return cb err if err
					cb null, localPermissions.concat childrenPermissions
		], (err, permissions)->
			return cb err if err
			callback null, permissions

	RoleSchema.statics.buildTree = (callback)->
		self = @
		index = 1
		self.find(parentId: null)
			.sort("name")
			.exec (err, parents)->
				return callback err if err
				async.eachSeries parents, (parent, cb)->
					self.rebuildTree parent, index, ->
						index = parent.rgt + 1
						cb err
				, (err)->
					callback err

	App.Models.Role = mongoose.model "Role", RoleSchema

	initCallback = ->
		Logger.info "RoleModel", "Init complete"
		callback null

	App.Models.Role.count (err, count)->
		throw err if err
		if count is 0
			App.Models.Role.create
				name: "admin"
				(err, role)->
					throw err if err
					App.Models.Role.buildTree ->
						do initCallback
		else
			App.Models.Role.buildTree ->
				do initCallback