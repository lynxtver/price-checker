module.exports = (callback = ->)->
	mongoose = require "mongoose"
	S = require "string"
	validator = require "mongoose-validator"
	async = require "async"
	path = require "path"
	bcrypt = require "bcrypt-nodejs"
	ObjectId = mongoose.Schema.Types.ObjectId

	App = require path.join "..", "App"

	Logger = App.Logger

	Logger.info "UserModel", "Init"

	UserSchema = new mongoose.Schema
		login:
			type: String
			required: 'error:common.required'
			trim: true
			index:
				unique: true
		name:
			type: String
		password: 
			type: String
			select: false
			required: 'error:common.required'
		admin: 
			type: Boolean
			default: false
		email:
			type: String
			required: 'error:common.required'
			lowercase: true
		recoveryKey:
			type: String
		roles: [
			type: ObjectId
			ref: "Role"
			unique: true
		]
		created:
			type: Date
			default: Date.now
		modified:
			type: Date
	,
		toObject: 
			virtuals: true
		toJSON: 
			virtuals: true
			transform: (doc, ret, options)->
				delete ret.password
				ret

	UserSchema.virtual "passwordConfirm"
		.set (value)->
			@_passwordConfirm = value

	UserSchema.path("email").validate validator
		message: "error:common.email"
		validator: "isEmail"
		
	UserSchema.path("email").validate (email, respond)->
		App.Models.User.findOne {email: email}, (err, user)=>
			throw err if err
			if @isNew
				respond not user?
			else
				respond not user? or user.id is @id
	, "error:users.existsWithEmail"

	UserSchema.path("login").validate (login, respond)->
		App.Models.User.findOne {login: login}, (err, user)=>
			throw err if err
			if @isNew
				respond not user?
			else
				respond not user? or user.id is @id
	, "error:users.existsWithLogin"

	UserSchema.path("password").validate (password)->
		if @isNew or @isModified "password"
			unless password
				@invalidate "password", "error:common.required"
			else unless S(password).trim().s
				@invalidate "password", "error:common.required"
			else unless password is @_passwordConfirm
				@invalidate "passwordConfirm", "error:users.passwordsNotMatch"
		true
	, null

	UserSchema.pre "save", (next)->
		if not @isNew and @isModified()
			@modified = Date.now()

		if @isNew or @isModified "password"
			salt = bcrypt.genSaltSync()
			@password = bcrypt.hashSync @password, salt

		do next

	UserSchema.statics.getById = (id, cb)-> 
		@findById id
			.populate "roles"
			.exec cb

	UserSchema.statics.auth = (login, password, callback)->
		@findOne
			login: login
		.select("+password")
		.exec (err, user)->
			return callback err if err
			return callback err, null unless user?
			callback err, if bcrypt.compareSync password, user.password then user else null

	UserSchema.methods.getPermissions = (callback)->
		if @admin
			App.Models.Permission.find {}, "name", (err, permissions)->
				return callback err if err
				async.map permissions, (permission, cb)->
					cb null, permission.name
				, callback
		else
			async.reduce @roles, [], (permissions, role, cb)->
				role.getPermissions (err, rolePermissions)->
					return cb err if err
					async.each rolePermissions, (rolePermission, cb)->
						permissions.push rolePermission unless permissions.some (permission)->permission is rolePermission
						cb null
					, (err)->
						cb null, permissions
			, callback

	UserSchema.methods.getSubordinates = (callback)->

		self = @

		async.reduce @roles, [], (roles, role, cb)->
			role.descendants (err, descendantRoles)->
				async.map descendantRoles, (descendantRole, cb)->
					cb null, descendantRole.id
				, (err, descendantRolesId)->
					return cb err if err
					roles = roles.concat descendantRolesId
					cb null, roles
		, (err, roles)->
			return callback err if err
			App.Models.User.find 
				$or: [
					_id: self.id
				,
					roles: $in: roles
				]
			.exec callback

	App.Models.User = mongoose.model "User", UserSchema

	initCallback = ->
		Logger.info "UserModel", "Init complete"
		callback null

	App.Models.User.count (err, count)->
		throw err if err
		if count is 0
			App.Models.User.create
				login: "admin"
				password: "admin"
				passwordConfirm: "admin"
				admin: true
				email: "admin@price-checker.net"
				(err, admin)->
					throw err if err
					do initCallback
		else
			do initCallback
