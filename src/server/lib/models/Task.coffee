module.exports = (callback = ->)->
	mongoose = require "mongoose"
	path = require "path"
	async = require "async"
	ObjectId = mongoose.Schema.Types.ObjectId

	App = require path.join "..", "App"

	Logger = App.Logger

	Logger.info "TaskModel", "Init"

	TaskSchema = new mongoose.Schema
		started: 
			type: Date
			required: true
		ended:
			type: Date
		grabbed:
			type: Number
			default: 0
		errorsNumber:
			type: Number
			default: 0
	,
		toObject: 
			virtuals: true
		toJSON: 
			virtuals: true

	App.Models.Task = mongoose.model "Task", TaskSchema

	initCallback = ->
		Logger.info "TaskModel", "Init complete"
		callback null

	do callback