module.exports = (callback = ->)->
	mongoose = require "mongoose"
	path = require "path"
	async = require "async"

	App = require path.join "..", "App"

	Logger = App.Logger

	Logger.info "ShopModel", "Init"

	ShopSchema = new mongoose.Schema
		name:
			type: String
			required: 'error:common.required'
		selector:
			type: String
			required: 'error:common.required'
	,
		toObject: 
			virtuals: true
		toJSON: 
			virtuals: true

	App.Models.Shop = mongoose.model "Shop", ShopSchema

	initCallback = ->
		Logger.info "ShopModel", "Init complete"
		callback null

	do callback