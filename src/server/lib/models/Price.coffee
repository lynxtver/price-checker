module.exports = (callback = ->)->
	mongoose = require "mongoose"
	path = require "path"
	async = require "async"
	ObjectId = mongoose.Schema.Types.ObjectId

	App = require path.join "..", "App"

	Logger = App.Logger

	Logger.info "PriceModel", "Init"

	PriceSchema = new mongoose.Schema
		timestamp: 
			type: Date
			required: true
		shop:
			type: ObjectId
			ref: "Shop"
			required: true
		good:
			type: ObjectId
			ref: "Good"
			required: true
		price:
			type: Number
		prevPrice:
			type: Number
	,
		toObject: 
			virtuals: true
		toJSON: 
			virtuals: true

	App.Models.Price = mongoose.model "Price", PriceSchema

	initCallback = ->
		Logger.info "PriceModel", "Init complete"
		callback null

	do callback