path = require "path"

App = require path.join "..", "App"

Logger = App.Logger

class Service
	
	constructor:->
		Logger.debug "#{@}", "constructor"
		do @initialize

	initialize:->
		Logger.info "#{@}", "init"
	
	toString:->
		"#{@constructor.name}"

module.exports = Service