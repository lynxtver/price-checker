path = require "path"
fs = require "fs"

App = require path.join "..", "App"

App.Services = {}

files = fs.readdirSync __dirname

for file in files
	fileName = path.basename file, ".js"
	unless path.basename(file) is path.basename(__filename)
		App.Services[fileName] = require path.join __dirname, fileName