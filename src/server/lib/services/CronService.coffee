path = require "path"
cron = require "cron"
CronJob = cron.CronJob

App = require path.join "..", "App"

CheckPrice = require path.join "..", "jobs", "CheckPrice"

Service = require path.join __dirname, "Service"

Logger = App.Logger

class CronService extends Service

	initialize:->
		super
		@checker = new CheckPrice
		@job = new CronJob App.Config.jobs.check, @task
		do @job.start

	task:=>
		Logger.info "#{@}", "Task run"
		do @checker.run

module.exports = new CronService