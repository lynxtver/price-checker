async = require "async"
path = require "path"
request = require "request"
cheerio = require "cheerio"
find = require "cheerio-eq"
trim = require "trim"

App = require path.join "..", "App"

Logger = App.Logger

class CheckPriceJob

	constructor:->
		@started = false

	run:->
		self = @
		unless @started
			@started = true
			Logger.info "#{self}", "Task started"
			async.waterfall [
				(cb)->
					task = new App.Models.Task
					task.started = +new Date
					task.save (err)->
						cb err, task

				(task, cb)->
					Logger.info "#{self}", "Fetch goods"
					App.Models.Good
						.find {}
						.populate "links.shop"
						.exec (err, goods)->
							cb err, task, goods

				(task, goods, cb)->
					checks = []
					Logger.info "#{self}", "Fetch links"
					async.eachSeries goods, (good, cb)->
						async.eachSeries good.links, (link, cb)->
							Logger.info "#{self}", "#{good.name}", "#{link.shop.name}", "#{link.url}"
							App.Models.Price
								.find
									good: good.id
									shop: link.shop.id
								.sort
									timestamp: -1
								.limit 2
								.exec (err, prevPrices)->
									if prevPrices.length > 0
										prevPrice = prevPrices[0]
									else
										prevPrice = null
									checks.push 
										good: good.id
										shop: link.shop.id
										url: link.url
										selector: link.shop.selector
										price: null
										prevPrice: prevPrice?.price
										prevPriceModel: prevPrice ? null
										prevPricesLength: prevPrices.length
									cb null
						, (err)->
							cb null
					, (err)-> 
						cb null, task, checks

				(task, checks, cb)->
					Logger.info "#{self}", "Fetch prices"
					async.eachSeries checks, (check, cb)->
						Logger.info "#{self}", "Fetching price for", "good: #{check.good}", "shop: #{check.shop}", "url: #{check.url}"
						self.getPrice check, (err)->
							Logger.info "#{self}", "Fetched price #{check.price} for", "good: #{check.good}", "shop: #{check.shop}", "url: #{check.url}"
							if check.price?
								task.grabbed++

								if check.prevPricesLength < 2 or 
								not check.prevPrice? or 
								(check.prevPrice? and check.prevPrice isnt check.price) or
								(check.prevPriceModel.price isnt check.prevPriceModel.prevPrice)
									price = new App.Models.Price check
									price.save (err)->
										cb err
								else
									if check.prevPriceModel?
										check.prevPriceModel.timestamp = +new Date
										check.prevPriceModel.save (err)->
											cb err
									else
										cb err
							else
								task.errorsNumber++
								cb null

					, (err)->
						cb null, task

				(task, cb)->
					task.ended = +new Date
					task.save (err)->
						cb null
			], (err)->
				Logger.error err if err
				Logger.info "#{self}", "Task finished"
				self.started = false


	getPrice:(check, cb)->
		request
			url: check.url
			method: "GET"
			gzip: true
			(err, response, body)->
				unless err
					try
						$ = cheerio.load(body)
						priceStr = trim find($, check.selector).text()
						price = priceStr.replace /[^0-9.]/g, ''
						price = parseInt price, 10
						if isNaN price
							Logger.error "Price parse error", "url: #{check.url}", "selector: #{check.selector}", "text: #{priceStr}"
							price = null
					catch err
						Logger.error err.toString(), "url: #{check.url}", "selector: #{check.selector}"
						price = null
					check.price = price
				check.timestamp = +new Date
				cb null

	toString:->
		"#{@constructor.name}"

module.exports = CheckPriceJob