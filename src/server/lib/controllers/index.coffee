path = require "path"
fs = require "fs"

App = require path.join "..", "App"

App.Controllers = {}

files = fs.readdirSync __dirname

for file in files
	fileName = path.basename file, ".js"
	unless path.basename(file) is path.basename(__filename)
		App.Controllers[fileName] = require path.join __dirname, fileName

for name, controller of App.Controllers
	controller.initialize?()