path = require "path"
_ = require "underscore"
moment = require "moment"
mongoose = require "mongoose"
ObjectId = mongoose.Types.ObjectId

App = require path.join "..", "App"

Logger = App.Logger

class BaseController

	constructor:->

	initialize:->
		Logger.info "#{@}", "init"

	toString:->
		"#{@constructor.name}"

	checkXhr: (req, res, next)=>
		if not req.xhr 
			return next new App.Errors.ApiError "error:notXHRRequest"
		do next

	checkAuthentication: (req, res, next)=>
		unless req.user
			return next new App.Errors.ApiError "error:notAuthenticated", 403
		do next

	isAdmin: (req, res, next)->
		if req.user and req.user.admin
			do next
		else
			next new App.Errors.ApiError "err:notAuthenticated", 403

	allow: (req, res, permission)->
		unless req.user
			false
		else unless res.locals._permissions.some((_permission)->_permission is permission)
			false
		else
			true

	convertObjectToId:(object)->
		if object?
			if _.isObject object
				if object.id?
					object.id
				else
					null
			else
				if _.isEmpty object
					null
				else
					object
		else
			null

	checkPermission: (permissions, permission)->
		_.contains permissions, permission.toUpperCase() 

	checkPermissionCb: (permission)->
		self = @
		(req, res, next)->
			if self.checkPermission req.session.permissions, permission
				do next
			else
				return next new App.Errors.ApiError "error:sufficientPrivileges", 403

	encodeRFC5987ValueChars: (str)->
		encodeURIComponent(str).replace(/['()]/g, escape).replace(/\*/g, '%2A').replace(/%(?:7C|60|5E)/g, unescape)

	checkObjectId: (id)->
		return false unless id
		try
			objectId = new ObjectId id
		catch 
			return false
		return objectId?

	convertToObjectId: (id)->
		try
			objectId = new ObjectId id
		catch 
			return null
		return objectId

module.exports = BaseController