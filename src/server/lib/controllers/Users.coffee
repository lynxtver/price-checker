async = require "async"
path = require "path"
_ = require "underscore"

App = require path.join "..", "App"

BaseEntityController = require path.join __dirname, "BaseEntity"

Logger = App.Logger

class UsersController extends BaseEntityController

	model: App.Models.User

	sort: "name"

	searchBy: ["name", "login", "email"]

	createAttrs: ["email", "name", "login", "password", "passwordConfirm", "roles"]

	updateAttrs: ["email", "name", "login", "password", "passwordConfirm", "roles"]

	constructor:->
		super

	initialize:->
		super

		App.express.get "/api/users", @checkAuthentication, @getList, @sendList
		App.express.post "/api/users", @create, @sendEntity
		App.express.post "/api/users/validate", @validateNew
		App.express.get "/api/users/:id", @getEntity, @sendEntity
		App.express.put "/api/users/:id", @getEntity, @update, @sendEntity
		App.express.put "/api/users/:id/validate", @getEntity, @validateExisting
		App.express.delete "/api/users/:id", @getEntity, @checkDelete, @delete, @sendEntity

	getList:(req, res, next)=>
		unless req.query.subordinate
			super
		else
			req.user.getSubordinates (err, users)=>
				async.map users, (user, cb)->
					cb null, user.id
				, (err, usersIds)=>
					return next err if err
					req.cond = _id: $in: usersIds
					super req, res, next

	checkDelete:(req, res, next)->
		App.Models.User.count
			admin: true
			_id: $ne: req.entity.id
			(err, count)->
				return next err if err
				unless count
					return next new App.Errors.ApiError "error:users.lastAdmin"
				do next

module.exports = new UsersController