path = require "path"
_ = require "underscore"
moment = require "moment"
async = require "async"

App = require path.join "..", "App"

BaseEntityController = require path.join __dirname, "BaseEntity"

Logger = App.Logger

class GoodsController extends BaseEntityController

	model: App.Models.Good

	sort: "name"

	searchBy: ["name"]

	createAttrs: ["name", "links"]

	updateAttrs: ["name", "links"]

	constructor:->
		super

	initialize:->
		super

		App.express.get "/api/goods", @getList, @getPrices, @sendList
		App.express.post "/api/goods", @create, @sendEntity
		App.express.get "/api/goods/:id", @getEntity, @populateSingle, @sendEntity
		App.express.put "/api/goods/:id", @getEntity, @update, @sendEntity
		App.express.delete "/api/goods/:id", @getEntity, @delete, @deletePrices, @sendEntity
		App.express.get "/api/goods/:id/prices", @getPriceList, @sendList

	create:(req, res, next)->
		if req.body.links? and _.isArray req.body.links
			_.each req.body.links, (item)->
				item.shop = @convertObjectToId item.shop
			, @
		else
			req.body.links = []

		super

	update:(req, res, next)=>
		if req.body.links? and _.isArray req.body.links
			_.each req.body.links, (item)->
				item.shop = @convertObjectToId item.shop
			, @
		else
			req.body.sellers = []

		super

	populateSingle:(req, res, next)->
		App.Models.Good.populate req.entity, [
			path: "links.shop"
		], (err, data)->
			return next err if err
			do next

	deletePrices:(req, res, next)->
		App.Models.Price.remove
			good: req.entity
			(err, entity)->
				return next err if err
				do next

	getPrices:(req, res, next)=>
		self = @

		async.map req.list.data, (good, cb)->
			item = good.toJSON()
			App.Models.Price.aggregate
				$sort:
					timestamp: -1
			, 
				$match:
					good: self.convertToObjectId item.id
			,
				$limit: 100
			,
				$group:
					_id: 
						shop: "$shop"
					price:
						$first: "$price"
					prevPrice:
						$first: "$prevPrice"
					timestamp:
						$first: "$timestamp"
			,
				$project:
					_id: 0
					shop: "$_id.shop"
					price: "$price"
					prevPrice: "$prevPrice"
					timestamp: "$timestamp"
			
			.exec (err, lastPrices)->
				item.lastMinPrice = null

				if lastPrices.length > 0
					lastPrices = _.filter lastPrices, (price)->not _.isNull price.price

					minPrice = _.min lastPrices, (price)->price.price

					App.Models.Shop
						.populate minPrice,
							path: "shop"
							model: "Shop"
							(err, minPrice)->
								item.lastMinPrice = minPrice

								cb null, item
				else
					cb null, item
		, (err, data)->
			return next err if err
			req.list.data = data
			do next

	getPriceList:(req, res, next)=>
		self = @

		startDate = moment().subtract 1, 'weeks'

		async.waterfall [
			(cb)->
				App.Models.Price.aggregate
					$match: 
						$and: [
							good: self.convertToObjectId req.params.id
						,
							timestamp: 
								$gte: startDate.toDate()
						]
				,
					$sort:
						timestamp: 1
				,
					$project:
						_id: 0
						start: "$timestamp"
						shop: "$shop"
						price: "$price"
						h:
							$hour: "$timestamp"
						m:
							$minute: "$timestamp"
						s:
							$second: "$timestamp"
						ml:
							$millisecond: "$timestamp"
				,
					$project:
						_id: 0
						shop: "$shop"
						price: "$price"
						start: 
							$subtract: [
								"$start"
								$add: [
									"$ml"
								,
									$multiply: [
										"$s"
										1000
									]
								,
									$multiply: [
										"$m"
										60,
										1000
									]
								]
							]
				,
					$group:
						_id: 
							date: "$start"
							shop: "$shop"
						price:
							$max: "$price"
				,
					$project:
						_id: 0
						date: "$_id.date"
						shop: "$_id.shop"
						price: "$price"
				,
					$group:
						_id: "$shop"
						data:
							$push: 
								date: "$date"
								value: "$price"
				,
					$project:
						_id: 0
						shop: "$_id"
						data: "$data"

				.exec (err, prices)->
					cb err, prices

			(prices, cb)->
				async.map prices, (price, cb)->
					elem = 
						shop: price.shop
						data: []

					sorted = _.sortBy price.data, (item)->
						item.date

					async.each sorted, (item, cb)->
						if item.value?
							elem.data.push [+item.date, item.value]
						cb null
					, (err)->
						cb null, elem
				, (err, prices)->
					cb err, prices

			(prices, cb)->
				cb null, 
					items: prices	
					total: prices.length

		], (err, data)->
			req.list =
				data: data.items
				recordsTotal: data.total
				recordsFiltered: data.total

			do next

module.exports = new GoodsController