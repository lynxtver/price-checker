path = require "path"
_ = require "underscore"
S = require "string"
async = require "async"
mongoose = require "mongoose"
ValidationError = mongoose.Error.ValidationError

App = require path.join "..", "App"

BaseController = require path.join __dirname, "Base"

Logger = App.Logger

class BaseEntityController extends BaseController

	constructor:->
		super

	initialize:->
		super

	getEntity:(req, res, next)=>
		@model.findById req.params.id, (err, entity)->
			return next err if err
			unless entity?
				return next new App.Errors.ApiError "error:entities.notFound"
			req.entity = entity
			do next

	sendEntity:(req, res, next)->
		res.send req.entity

	sendSubEntity:(req, res, next)->
		res.send req.subEntity

	getList:(req, res, next)=>

		self = @

		if req.cond 
			cond = req.cond
		else
			cond = {}

		offset = if _.isFinite req.query.start then Math.abs parseInt req.query.start, 10 else 0
		limit = if _.isFinite req.query.length then Math.abs parseInt req.query.length, 10 else null
 
		if req.query.order and req.query.columns and _.isArray req.query.order
			sort = []
			useDefault = true
			for col, index in req.query.order
				if req.query.columns[col.column].name?
					if @defaultSort? and req.query.columns[col.column].name is @defaultSort
						useDefault = false
					fieldName = if self.model.schema.paths["#{req.query.columns[col.column].name}_sort"]? then "#{req.query.columns[col.column].name}_sort" else "#{req.query.columns[col.column].name}"
					sort.push if col.dir is "desc" then "-#{fieldName}" else "#{fieldName}"
			if @defaultSort? and useDefault
				sort.push @defaultSort
			sort = sort.join " "
		else
			sort = @sort ? ""

		if @searchBy and req.query.search?.value
			where = {}
			regExp = new RegExp "#{S(req.query.search.value).trim().s}", "i"
			if _.isArray @searchBy
				if @searchBy.length is 1
					where["#{@searchBy[0]}"] = regExp
				else
					where["$or"] = []
					for attr in @searchBy
						item = {}
						fieldName = if self.model.schema.paths["#{attr}_search"]? then "#{attr}_search" else "#{attr}"
						item[fieldName] = regExp
						where["$or"].push item
			else
				where["#{@searchBy}"] = regExp

		async.parallel [
			(cb)=>
				query = @model.find(cond)

				if where?
					query.where where

				if sort?
					query.sort sort

				query = query.limit(limit) if limit?

				query
					.skip(offset)
					.exec cb

			(cb)=> 
				query = @model.count(cond)

				if where?
					query.where where

				query.exec cb

			(cb)=>
				@model.count cond, cb

		], (err, results)->
			return next err if err
			[items, filtered, total] = results
			
			req.list =
				data: items
				recordsTotal: total
				recordsFiltered: filtered

			do next

	sendList:(req, res, next)->
		res.send req.list

	create:(req, res, next)=>

		data = _.pick req.body, @createAttrs

		entity = new @model data

		entity.validate (err)->
			return next err if err
			entity.save (err, entity)->
				return next err if err
				req.entity = entity
				do next

	validateNew:(req, res, next)=>

		data = _.pick req.body, @createAttrs
		entity = new @model data
		entity.validate (err)->
			if err
				return next err unless err instanceof ValidationError
				for attr, error of err.errors
					delete err.errors[attr] unless data[attr]?
				return next err unless _.isEmpty err.errors
				res.send {}
			else
				res.send {}

	validateExisting:(req, res, next)=>

		entity = req.entity
		data = _.pick req.body, @updateAttrs

		entity.set data

		entity.validate (err)->
			if err
				return next err unless err instanceof ValidationError
				for attr, error of err.errors
					delete err.errors[attr] unless data[attr]?
				return next err unless _.isEmpty err.errors
				res.send {}
			else
				res.send {}

	update:(req, res, next)=>
		entity = req.entity

		data = _.pick req.body, @updateAttrs

		entity.set data

		entity.validate (err)->
			return next err if err
			entity.save (err, entity)->
				return next err if err
				req.entity = entity
				do next

	delete:(req, res, next)->
		req.entity.remove (err, entity)->
			return next err if err
			req.entity = entity
			do next

	getSubEntity:(options)=>

		self = @
		(req, res, next)->
			entity = req.entity
			list = entity[options.name]
			subEntity = list.id req.params[options.param]
			unless subEntity?
				return next new App.Errors.ApiError "error:entities.notFound"

			req.subEntity = subEntity
			do next

	createSubEntity:(options)=>

		self = @
		(req, res, next)->
			entity = req.entity
			
			data = _.pick req.body, options.attrs
			length = entity[options.name].push data
			req.subEntity = entity[options.name][length - 1]

			entity.validate (err)->
				return next err if err
				entity.save (err, entity)->
					return next err if err
					do next

	updateSubEntity:(options)=>
		self = @
		(req, res, next)->
			subEntity = req.subEntity
			data = _.pick req.body, options.attrs
			subEntity.set data

			subEntity.parent().validate (err)->
				return next err if err
				subEntity.parent().save (err, entity)->
					return next err if err
					do next

	removeSubEntity:(options)=>
		self = @
		(req, res, next)->
			subEntity = req.subEntity
			subEntity.remove (err)->
				return next err if err
				subEntity.parent().save (err, entity)->
					return next err if err
					req.subEntity = {}
					do next

	getSubEntityList:(options)=>
		
		self = @

		(req, res, next)->

			entity = req.entity

			list = entity[options.name]

			if req.cond 
				cond = req.cond
			else
				cond = {}

			offset = if _.isFinite req.query.start then Math.abs parseInt req.query.start, 10 else 0
			limit = if _.isFinite req.query.length then Math.abs parseInt req.query.length, 10 else 10
	 
			if req.query.order and req.query.columns and _.isArray req.query.order
				sort = []
				for col, index in req.query.order
					if req.query.columns[col.column].name?
						item = 
							field: "#{req.query.columns[col.column].name}"
							type: if col.dir is "desc" then "desc" else "asc"

						sort.push item
			else
				sort = options.sort ? []

			where = []

			if options.searchBy and req.query.search?.value
				regExp = new RegExp "#{S(req.query.search.value).trim().s}", "i"
				if _.isArray options.searchBy
					if options.searchBy.length is 1
						where.push ["#{options.searchBy[0]}", regExp]
					else
						where = []
						for attr in options.searchBy
							where.push ["#{attr}", regExp]
				else
					where.push ["#{options.searchBy}", regExp]

			async.parallel [

				(cb)->
					async.waterfall [
						(cb)->
							if _.isArray(sort) and sort.length > 0
								field = sort[0].field
								type = sort[0].type
								list.sort (a, b)->
									if (a[field] ? "") > (b[field] ? "")
										if type is "desc" then -1 else 1
									else if (a[field] ? "") < (b[field] ? "")
										if type is "desc" then 1 else -1
									else
										0
								cb null, list
							else
								cb null, list

						(items, cb)->
							if where.length > 0
								async.filter items, (item, cb)->
									async.some where, (cond, cb)->
										cb cond[1].test item[cond[0]]
									, cb
								, (items)->
									cb null, items
							else
								cb null, items

						(items, cb)->
							if options.map
								async.map items, (item, cb)->
									map item, cb
								, cb
							else
								cb null, items
						
						(items, cb)->
							async.map items, (item, cb)->
								element = item.toJSON()
								element.id = item._id.toString()
								delete element._id
								cb null, element
							, cb
					], cb
				(cb)->
					cb null, list.length

			], (err, results)->
				return next err if err
				[items, total] = results

				req.list =
					data: items.slice offset, limit
					recordsTotal: total
					recordsFiltered: items.length

				do next

module.exports = BaseEntityController