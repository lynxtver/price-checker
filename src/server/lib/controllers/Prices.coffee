path = require "path"
_ = require "underscore"
moment = require "moment"
async = require "async"

App = require path.join "..", "App"

BaseEntityController = require path.join __dirname, "BaseEntity"

Logger = App.Logger

class PricesController extends BaseEntityController

	model: App.Models.Price

	sort: "timestamp"

	createAttrs: ["timestamp", "shop", "good", "price", "prevPrice"]

	updateAttrs: ["timestamp", "shop", "good", "price", "prevPrice"]

	constructor:->
		super

	initialize:->
		super

		App.express.get "/api/prices/summary/goods", @getSummaryPriceList, @sendList
		App.express.get "/api/prices/summary/shops", @getSummaryByShopsPriceList, @sendList

		App.express.get "/api/prices", @getList, @populateList, @sendList
		App.express.post "/api/prices", @create, @sendEntity
		App.express.get "/api/prices/:id", @getEntity, @populateSingle, @sendEntity
		App.express.put "/api/prices/:id", @getEntity, @update, @sendEntity
		App.express.delete "/api/prices/:id", @getEntity, @delete, @sendEntity

		App.express.get "/api/prices/good/:id", @getSummaryGoodPriceList, @sendList

	create:(req, res, next)->
		req.body.shop = @convertObjectToId req.body.shop
		req.body.good = @convertObjectToId req.body.good
		super

	update:(req, res, next)=>
		req.body.shop = @convertObjectToId req.body.shop
		req.body.good = @convertObjectToId req.body.good
		super

	getGoodPriceList:(req, res, next)->
		req.cond = 
			good: req.params.id
		do next

	populateList:(req, res, next)->
		App.Models.Price.populate req.list.data, [
			path: "shop"
		,
			path: "good"
		], (err, data)->
			return next err if err
			do next

	populateSingle:(req, res, next)->
		App.Models.Price.populate req.entity, [
			path: "shop"
		,
			path: "good"
		], (err, data)->
			return next err if err
			do next

	getSummaryPriceList:(req, res, next)=>
		self = @

		ascOrder = req.query.order?[0]?.dir is "asc"

		async.waterfall [
			(cb)->
				App.Models.Price.aggregate
					$sort:
						timestamp: -1
				,
					$group:
						_id: 
							good: "$good"
							shop: "$shop"
						price:
							$first: "$price"
						prevPrice:
							$first: "$prevPrice"
						timestamp:
							$first: "$timestamp"
				,
					$group:
						_id: "$_id.good"
						data:
							$addToSet:
								shop: "$_id.shop"
								price: "$price"
								prevPrice: "$prevPrice"
								timestamp: "$timestamp"
				,
					$project:
						_id: 0
						good: "$_id"
						data: "$data"

				.exec (err, prices)->
					cb err, prices

			(prices, cb)->
				async.each prices, (price, cb)->
					App.Models.Price.populate price,
						path: "good"
						model: "Good"
						(err, price)->
							cb null
				, (err)->
					cb err, prices

			(prices, cb)->
				async.filter prices, (price, cb)->
					cb price.good? and _.some price.data, (item)->
						item.price isnt item.prevPrice
				, (prices)->
					cb null, prices

			(prices, cb)->
				prices.sort (a, b)->
					if a.good.name > b .good.name
						return if ascOrder then 1 else -1
					else if a.good.name < b .good.name
						return if ascOrder then -1 else 1
					else 
						return 0

				cb null, prices

			(prices, cb)->
				cb null, 
					items: prices
					total: prices.length

		], (err, data)->
			req.list =
				data: data.items
				recordsTotal: data.total
				recordsFiltered: data.total

			do next

	getSummaryByShopsPriceList:(req, res, next)=>
		self = @

		ascOrder = req.query.order?[0]?.dir is "asc"

		async.waterfall [
			(cb)->
				App.Models.Price.aggregate
					$sort:
						timestamp: -1
				,
					$group:
						_id: 
							good: "$good"
							shop: "$shop"
						price:
							$first: "$price"
						prevPrice:
							$first: "$prevPrice"
						timestamp:
							$first: "$timestamp"
				,
					$group:
						_id: "$_id.shop"
						data:
							$addToSet:
								good: "$_id.good"
								price: "$price"
								prevPrice: "$prevPrice"
								timestamp: "$timestamp"
				,
					$project:
						_id: 0
						shop: "$_id"
						data: "$data"

				.exec (err, prices)->
					cb err, prices

			(prices, cb)->
				async.each prices, (price, cb)->
					App.Models.Price.populate price,
						path: "shop"
						model: "Shop"
						(err, price)->
							cb null
				, (err)->
					cb err, prices

			(prices, cb)->
				async.filter prices, (price, cb)->
					cb price.shop?
				, (prices)->
					cb null, prices

			(prices, cb)->
				prices.sort (a, b)->
					if a.shop.name > b .shop.name
						return if ascOrder then 1 else -1
					else if a.shop.name < b .shop.name
						return if ascOrder then -1 else 1
					else 
						return 0

				cb null, prices

			(prices, cb)->
				cb null, 
					items: prices
					total: prices.length

		], (err, data)->
			req.list =
				data: data.items
				recordsTotal: data.total
				recordsFiltered: data.total

			do next

	getSummaryGoodPriceList:(req, res, next)=>
		self = @

		sortOrder = req.query.order?[0]?.dir is "asc"
		sortFunc = switch req.query.order?[0]?.column
			when "3" then (a, b)->
				if a.price > b.price
					return if sortOrder then 1 else -1
				else if a.price < b.price
					return if sortOrder then -1 else 1
				else 
					return 0
		
			else (a, b)->
				if a.shop.name > b.shop.name
					return if sortOrder then 1 else -1
				else if a.shop.name < b.shop.name
					return if sortOrder then -1 else 1
				else 
					return 0

		async.waterfall [
			(cb)->
				App.Models.Price.aggregate
					$match:
						good: self.convertToObjectId req.params.id
				,
					$sort:
						timestamp: -1
				,
					$group:
						_id: "$shop"
						price:
							$first: "$price"
						prevPrice:
							$first: "$prevPrice"
						timestamp:
							$first: "$timestamp"
				,
					$project:
						_id: 0
						shop: "$_id"
						price: "$price"
						timestamp: "$timestamp"
						prevPrice: "$prevPrice"

				.exec (err, prices)->
					cb err, prices

			(prices, cb)->
				async.each prices, (price, cb)->
					App.Models.Price.populate price,
						path: "shop"
						model: "Shop"
						(err, price)->
							cb null
				, (err)->
					cb err, prices

			(prices, cb)->
				prices.sort sortFunc

				cb null, prices

			(prices, cb)->
				cb null, 
					items: prices
					total: prices.length

		], (err, data)->
			req.list =
				data: data.items
				recordsTotal: data.total
				recordsFiltered: data.total

			do next

module.exports = new PricesController