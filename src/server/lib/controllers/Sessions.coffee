path = require "path"
passport = require "passport"

App = require path.join "..", "App"

BaseController = require path.join __dirname, "Base"

Logger = App.Logger

class SessionController extends BaseController
	
	constructor:->
		super

	initialize:->
		super

		App.express.get "/api/session", @send
		App.express.post "/api/session", @login, @send
		App.express.put "/api/session", @login, @send
		App.express.delete "/api/session", @logout

	send:(req, res, next)->
		user = null
		if req.user
			user = req.user.toJSON()
			delete user.password
		res.send 
			id: req.session.id
			user: user
			token: req.csrfToken()
			permissions: req.session.permissions ? []

	logout:(req, res, next)->
		res.clearCookie "remember_me"
		req.logout()
		res.send 
			user: null
			token: req.csrfToken()

	login:(req, res, next)->
		(passport.authenticate 'local', badRequestMessage: "error:misingCredentials", (err, user, info)->
			return next err if err
			unless user
				res.status 400
				res.send
					error:
						formErrors: info.message ? info
			else
				req.logIn user, (err)->
					return next err if err
					if req.body.rememberme
						App.Models.Token.setToken req.user.id, (err, token)->
							return next err if err
							res.cookie 'remember_me', token.hash,
								path: '/'
								httpOnly: true
								maxAge: 1000 * 60 * 60 * 24 * 7
							do next
					else
						do next
		)(req, res, next)

module.exports = new SessionController