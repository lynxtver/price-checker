path = require "path"
_ = require "underscore"

App = require path.join "..", "App"

BaseHierarchyEntityController = require path.join __dirname, "BaseHierarchyEntity"

Logger = App.Logger

class RolesController extends BaseHierarchyEntityController

	model: App.Models.Role

	searchBy: ["name"]

	createAttrs: ["name", "parentId", "permissions"]

	updateAttrs: ["name", "parentId", "permissions"]

	constructor:->
		super

	initialize:->
		super

		App.express.get "/api/roles", @getHierarchy, @sendList
		App.express.post "/api/roles", @create, @sendEntity
		App.express.get "/api/roles/:id", @getEntity, @sendEntity
		App.express.put "/api/roles/:id", @getEntity, @update, @sendEntity
		App.express.delete "/api/roles/:id", @getEntity, @checkDelete, @delete, @sendEntity		
		
	checkDelete:(req, res, next)->
		entity = req.entity
		unless entity.isLeaf()
			return next new App.Errors.ApiError "error:hierarchy.notLeaf"
		do next

module.exports = new RolesController