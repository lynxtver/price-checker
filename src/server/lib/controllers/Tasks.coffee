path = require "path"
_ = require "underscore"
moment = require "moment"
async = require "async"

App = require path.join "..", "App"

BaseEntityController = require path.join __dirname, "BaseEntity"

Logger = App.Logger

class TasksController extends BaseEntityController

	model: App.Models.Task

	constructor:->
		super

	initialize:->
		super

		App.express.get "/api/tasks", @getList, @sendList

module.exports = new TasksController