path = require "path"
_ = require "underscore"
moment = require "moment"
async = require "async"

App = require path.join "..", "App"

BaseEntityController = require path.join __dirname, "BaseEntity"

Logger = App.Logger

class ShopsController extends BaseEntityController

	model: App.Models.Shop

	sort: "name"

	searchBy: ["name"]

	createAttrs: ["name", "selector"]

	updateAttrs: ["name", "selector"]

	constructor:->
		super

	initialize:->
		super

		App.express.get "/api/shops", @getList, @sendList
		App.express.post "/api/shops", @create, @sendEntity
		App.express.get "/api/shops/:id", @getEntity, @sendEntity
		App.express.put "/api/shops/:id", @getEntity, @update, @sendEntity
		App.express.delete "/api/shops/:id", @getEntity, @delete, @sendEntity
		App.express.get "/api/shops/:id/prices", @getPriceList, @sendList

	getPriceList:(req, res, next)=>
		self = @

		startDate = moment().subtract 1, 'weeks'

		async.waterfall [
			(cb)->
				App.Models.Price.aggregate
					$match: 
						$and: [
							shop: self.convertToObjectId req.params.id
						,
							timestamp: 
								$gte: startDate.toDate()
						]
				,
					$sort:
						timestamp: 1
				,
					$project:
						_id: 0
						start: "$timestamp"
						good: "$good"
						price: "$price"
						h:
							$hour: "$timestamp"
						m:
							$minute: "$timestamp"
						s:
							$second: "$timestamp"
						ml:
							$millisecond: "$timestamp"
				,
					$project:
						_id: 0
						good: "$good"
						price: "$price"
						start: 
							$subtract: [
								"$start"
								$add: [
									"$ml"
								,
									$multiply: [
										"$s"
										1000
									]
								,
									$multiply: [
										"$m"
										60,
										1000
									]
								]
							]
				,
					$group:
						_id: 
							date: "$start"
							good: "$good"
						price:
							$max: "$price"
				,
					$project:
						_id: 0
						date: "$_id.date"
						good: "$_id.good"
						price: "$price"
				,
					$group:
						_id: "$good"
						data:
							$push: 
								date: "$date"
								value: "$price"
				,
					$project:
						_id: 0
						good: "$_id"
						data: "$data"

				.exec (err, prices)->
					cb err, prices

			(prices, cb)->
				async.map prices, (price, cb)->
					elem = 
						good: price.good
						data: []

					sorted = _.sortBy price.data, (item)->
						item.date

					async.each sorted, (item, cb)->
						if item.value?
							elem.data.push [+item.date, item.value]
						cb null
					, (err)->
						cb null, elem
				, (err, prices)->
					cb err, prices

			(prices, cb)->
				cb null, 
					items: prices	
					total: prices.length

		], (err, data)->
			req.list =
				data: data.items
				recordsTotal: data.total
				recordsFiltered: data.total

			do next

module.exports = new ShopsController