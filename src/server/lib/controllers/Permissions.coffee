path = require "path"
_ = require "underscore"

App = require path.join "..", "App"

BaseEntityController = require path.join __dirname, "BaseEntity"

Logger = App.Logger

class PermissionsController extends BaseEntityController

	model: App.Models.Permission

	sort: "name"

	searchBy: ["name"]

	constructor:->
		super

	initialize:->
		super

		App.express.get "/api/permissions", @getList, @sendList
		App.express.get "/api/permissions/:id", @getEntity, @sendEntity

module.exports = new PermissionsController