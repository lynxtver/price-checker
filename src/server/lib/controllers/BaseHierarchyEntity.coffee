path = require "path"
_ = require "underscore"
S = require "string"
async = require "async"

App = require path.join "..", "App"

BaseEntityController = require path.join __dirname, "BaseEntity"

Logger = App.Logger

class BaseHierarchyEntityController extends BaseEntityController

	constructor:->
		super

	initialize:->
		super

	getList:(req, res, next)=>
		self = @
		
		if req.query.hierarchy
			@getHierarchy req, res, next
		else

			async.waterfall [
				(cb)=> super req, res, cb
				(cb)-> self.countChildren req, res, cb
			], next

	create:(req, res, next)=>
		self = @
		super req, res, (err)->
			return next err if err
			self.model.buildTree next

	update:(req, res, next)=>
		self = @
		super req, res, (err)->
			return next err if err
			self.model.buildTree next

	delete:(req, res, next)=>
		self = @
		super req, res, (err)->
			return next err if err
			self.model.buildTree next

	countChildren:(req, res, next)->
		async.map req.list.data, (item, cb)->
			obj = item.toObject()
			item.children (err, children)->
				return cb err if err
				if children.length > 0
					async.filter children, (item, cb)->
						cb not item.deleted
					, (children)->
						obj.childrenCount = children.length
						cb null, obj
				else
					obj.childrenCount = children.length
					cb null, obj
		, (err, data)->
			return next err if err
			req.list.data = data
			do next

	getHierarchy:(req, res, next)=>
		self = @

		if req.cond 
			cond = req.cond
		else
			cond = {}

		parentId = if @checkObjectId req.query.parentId then req.query.parentId else null

		offset = if _.isFinite req.query.start then Math.abs parseInt req.query.start, 10 else 0
		limit = if _.isFinite req.query.length then Math.abs parseInt req.query.length, 10 else 10

		if @searchBy and req.query.search?.value
			where = {}
			regExp = new RegExp "#{S(req.query.search.value).trim().s}", "i"
			if _.isArray @searchBy
				if @searchBy.length is 1
					where["#{@searchBy[0]}"] = regExp
				else
					where["$or"] = []
					for attr in @searchBy
						item = {}
						item["#{attr}"] = regExp
						where["$or"].push item
			else
				where["#{@searchBy}"] = regExp

		async.waterfall [
			(cb)->
				query = self.model.find(cond)
					.sort("lft name")

				if where
					query.where where
					
				query.exec cb

			(items, cb)->
				if parentId
					self.model.findById parentId, (err, parent)->
						cb err, items, parent
				else
					cb null, items, null

			(items, parent, cb)->
				if parent
					async.filter items, (item, cb)->
						cb item.id.toString() is parent.id.toString() or item.isDescendantOf parent
					, (items)->
						cb null, items, parent
				else
					cb null, items, null

			(items, parent, cb)->
				if parent
					async.map items, (item, cb)->
						item.selfAndAncestors (err, list)->
							async.filter list, (item, cb)->
								cb item.id.toString() is parent.id.toString() or item.isDescendantOf parent
							, (items)->
								cb null, items
					, cb
				else
					async.map items, (item, cb)->
						item.selfAndAncestors cb
					, cb

			(items, cb)->
				async.map items, (list, cb)->
					async.sortBy list, (item, cb)->
						cb null, item.lft
					, cb
				, cb

			(items, cb)->
				async.reduce items, [], (memo, list, cb)->
					children = memo
					for item, index in list
						child = _.find children, (i)-> i.id.toString() is item.id.toString()
						unless child?
							child = item.toObject()
							child.children = []
							children.push child
						children = child.children

					cb null, memo
				, cb

		], (err, items)->
			return callback err if err
			req.list = 
				data: items
			do next

module.exports = BaseHierarchyEntityController