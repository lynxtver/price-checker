winston = require "winston"
moment = require "moment"
fs = require "fs"

App = require "./lib/App"

logDateFormat = ->
	moment().format App.Config.dateFormat.full

consoleLog = new winston.transports.Console
	colorize: true
	timestamp: logDateFormat
	handleExceptions: true
	level: App.Config.logLevel

try
	fs.mkdirSync "#{__dirname}/log"
catch 

allLog = new winston.transports.DailyRotateFile
	dirname: "#{__dirname}/log/"
	name: "all"
	filename: "all.log"
	handleExceptions: true
	timestamp: logDateFormat
	json: false
	maxsize: 5 * 1024 * 1024 
	maxFiles: 10
	level: App.Config.logLevel

errorsLog = new winston.transports.DailyRotateFile
	dirname: "#{__dirname}/log/"
	name: "errors"
	filename: "error.log"
	handleExceptions: true
	timestamp: logDateFormat
	json: false
	maxsize: 5 * 1024 * 1024 
	maxFiles: 10
	level: "error"

module.exports = new winston.Logger
	transports: [consoleLog, allLog, errorsLog]