cluster = require "cluster"
numCPUs = require('os').cpus().length

createWorker = ->
	workersCount = Object.keys(cluster.workers).length
	if workersCount < numCPUs
		console.log "Fork new worker"
		worker = cluster.fork()
		worker.on "message", (msg = {})->
			if msg.type? and msg.type is "online"
				console.log "Worker #{msg.pid} online"
				do createWorker

if cluster.isMaster

	cluster.setupMaster
		exec: "server.js"
		silent: false

	cluster.on 'exit', (worker, code, signal)->
		console.log "Worker #{worker.process.pid} died"
		do createWorker

	do createWorker