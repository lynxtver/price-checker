path = require "path"
async = require "async"
i18n = require "i18next"
cluster = require "cluster"
http = require "http"
express = require "express"
session = require "express-session"
bodyParser = require "body-parser"
multer  = require "multer"
csrf = require "csurf"
favicon = require "serve-favicon"
cookieParser = require "cookie-parser"
compression = require "compression"
methodOverride = require "method-override"
MongoStore = require('connect-mongo')(session)
expressWinston = require "express-winston"
passport = require "passport"
LocalStrategy = require("passport-local").Strategy
RememberMeStrategy = require("passport-remember-me").Strategy
moment = require "moment"
mongoose = require "mongoose"
ValidationError = mongoose.Error.ValidationError

App = require path.join __dirname, "lib", "App"
App.Config = require path.join __dirname, "conf", "config"
App.Config.serverRoot = __dirname
Logger = App.Logger = require path.join __dirname, "logger"

mongoConfig = App.Config.mongo

mongoose.set "debug", App.Config.debug

stopServer = ->
	Logger.info "Cleanup"
	App.server.close ->
		Logger.info "HTTP server closed"
		mongoose.disconnect ->
			Logger.info "DB connection closed"
			process.exit()

async.waterfall [
	(cb)->
		App.DB = mongoose.connect "mongodb://#{mongoConfig.username}:#{mongoConfig.password}@#{mongoConfig.host}/#{mongoConfig.db}", 
			server: 
				auto_reconnect: mongoConfig.auto_reconnect
			(err)->
				App.Logger.debug "Connected to DB"
				cb err
	(cb)-> 
		require path.join __dirname, "lib", "errors"
		cb null
	(cb)-> 
		require( path.join __dirname, "lib", "models") cb
], (err)->
	if err
		Logger.error err.toString()
		process.exit 0

	sessionStore = new MongoStore
		db: mongoose.connection.db
		stringify: false

	App.express = app = express()

	app.disable "x-powered-by"

	app.set 'port', App.Config.server.port ? 3000

	i18n.init
		resGetPath: path.join App.Config.serverRoot, "public", "locales", "__lng__", "__ns__.json"
		ignoreRoutes: ['img/', 'css/', 'js/', 'fonts/']
		fallbackLng: 'en'
		postProcess: "sprintf"
		debug: true
		ns:
			namespaces: ['common', 'error']
			defaultNs: 'common'

	passport.use new LocalStrategy 
		usernameField: "login"
		passwordField: "password"
		(login, password, done)->
			App.Models.User.auth login, password, (err, user)->
				done err if err
				if user
					done null, user
				else
					done null, false, "error:users.incorectPassword"

	passport.use new RememberMeStrategy (hash, done)->
		App.Models.Token.getToken hash, (err, id)->
			return done err if err
			return done null, false unless id

			App.Models.User.getById id, (err, user)->
				return done err if err
				return done null, false unless user?
				done null, user
	, (user, done)->
		App.Models.Token.issueToken user, done

	passport.serializeUser (user, done)->
		done null, user.id

	passport.deserializeUser (id, done)->
		App.Models.User.getById id, done

	if App.Config.proxy
		app.enable "trust proxy"
	else
		app.use express.static path.join __dirname, "public"

	app.use compression()
	#app.use favicon()
	app.use cookieParser App.Config.session.secret

	app.use expressWinston.logger
		transports: [Logger]

	app.use bodyParser.urlencoded
		extended: true
	app.use bodyParser.json()

	app.use multer()

	app.use i18n.handle

	app.use session
		cookie:
			maxAge: 30 * 60 * 1000
			expires: 30 * 60 * 1000
			httpOnly: true
		resave: true
		saveUninitialized: true
		key: App.Config.session.sessionKey
		secret: App.Config.session.secret
		store: sessionStore
		rolling: true

	app.use csrf()
	app.use methodOverride()

	app.use passport.initialize()
	app.use passport.session()
	app.use passport.authenticate "remember-me"

	app.use require "express-domain-middleware"

	app.all "*", (req, res, next)->
		if req.user
			req.user.getPermissions (err, permissions)->
				return next err if err
				req.session.permissions = permissions
				do next
		else
			req.session.permissions = []
			do next

	require path.join __dirname, "lib", "controllers"
	require path.join __dirname, "lib", "services"
	
	app.use (req, res, next)->
		next new App.Errors.ApiError "error:unknownRequest"

	app.use (err, req, res, next)->
		status = 500
		if err
			status = err.status if err.status
			if err instanceof ValidationError
				status = 400
			Logger.error err, req.url
			res.status status
			res.send
				error: err

	Logger.info "Server", "Starting server at port #{app.get 'port'}"

	App.server = http.createServer(app).listen(app.get "port")

	process.on 'SIGINT', stopServer
	process.on 'SIGTERM', stopServer

	if cluster.isWorker
		Logger.info "Server", "Cluster mode"
		process.send
			type: "online"
			pid: process.pid