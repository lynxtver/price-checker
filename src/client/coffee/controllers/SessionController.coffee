define [
	"controllers/BaseController"
	"config"
	"layouts/LoginLayout"
	"views/LoginFormView"
], (BaseController, Config, LoginLayout, LoginFormView)->

	class SessionController extends BaseController

		login:->
			if Config.App.Session.getUser()?
				Backbone.history.navigate "/", true
			else
				loginForm = new LoginFormView
					model: Config.App.Session

				Config.App.setLayout LoginLayout
				Config.App.getLayout().content.show loginForm

		logout:->
			Config.App.Session.destroy wait: true
				.always ->
					Backbone.history.navigate "/", true