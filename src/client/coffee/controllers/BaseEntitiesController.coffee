define [
	"controllers/BaseController"
	"config"
	"layouts/GridLayout"
	"layouts/PanelLayout"
	"views/BreadcrumbsView"
	"layouts/ModalLayout"
	"views/entities/base/ListView"
], (BaseController, Config, GridLayout, PanelLayout, BreadcrumbsView, ModalLayout,
	ListView)->

	class BaseEntitiesController extends BaseController

		view: ListView

		initialize:->
			Config.App.reqres.setHandler "#{@entityName}:remove", @onRemoveEntity

		onDestroy:->
			Config.App.reqres.removeHandler "#{@entityName}:remove"

		onRemoveEntity:(id)=>
			self = @
			defer = $.Deferred()			
			if Config.App.Session.checkPermissions "#{@entityName}.remove"
				modal = new ModalLayout
					title: "common:modal.delete.title"
					contentText: "common:modal.delete.content"
					successText: "common:actions.delete"
					successColor: "danger"
					autoClose: true
					onConfirm:->
						model = new self.model id: id
						model.destroy
							wait:true
							success:->
								do modal.close
								do defer.resolve
							error:(model, response, options)->
								Config.App.showError model, response, options
								do defer.reject

					onCancel:->
						do defer.reject

				Config.App.showModal modal
			else
				do defer.reject
			defer.promise()

		setupLayout:->
			grid = new GridLayout
				rows: [
					columns: [
						name: "breadcrumbs"
					]
				,
					columns: [
						name: "content"
					]
				]
			do Config.App.setDefaultLayout
			Config.App.getLayout().content.show grid

		loadEntity:(id)->
			deferred = $.Deferred()
			model = new @model id: id

			model.fetch()
				.done ->
					deferred.resolve model
				.fail ->
					deferred.reject

			do deferred.promise

		viewEntity:(id)->
			self = @
			args = _.rest arguments
			do @setupLayout
			@loadEntity id
				.done (model)->
					self.showEntity.apply self, [model].concat args
				.fail ->
					do self.list

		list:->
			entitiesView = new @view
				entityName: @entityName
			entitiesListView = new @listView
			panel = new PanelLayout
				title: @listTitle

			do @setupLayout

			breadcrumbs = new BreadcrumbsView
				items: [
					icon: "dashboard"
					name: "common:dashboard"
					url: "/"
				,
					icon: @icons
					name: @listTitle
					active: true
				]

			Config.App.getLayout().content.currentView.breadcrumbs.show breadcrumbs
			Config.App.getLayout().content.currentView.content.show panel
			panel.content.show entitiesView
			entitiesView.list.show entitiesListView

		addEdit:(id)->
			self = @
			do @setupLayout
			model = new @model id: id
			unless id
				@showEntityForm model
			else
				model.fetch()
					.done ->
						self.showEntityForm model
					.fail ->
						do self.list

		showEntity:(model)->

			breadcrumbsItems = [
				icon: "dashboard"
				name: "common:dashboard"
				url: "/"
			,
				icon: @icons
				name: @listTitle
				url: "/#{@entityName}"
			,
				icon: @icon
				name: model.getBreadcrumbName()
				translate: no
				active: true
			]

			breadcrumbs = new BreadcrumbsView
				items: breadcrumbsItems

			panel = new PanelLayout
				title: @formTitle

			view = new @singleView
				model: model

			Config.App.getLayout().content.currentView.breadcrumbs.show breadcrumbs
			Config.App.getLayout().content.currentView.content.show panel
			panel.content.show view

		showEntityForm:(model)->

			breadcrumbsItems = [
				icon: "dashboard"
				name: "common:dashboard"
				url: "/"
			,
				icon: @icons
				name: @listTitle
				url: "/#{@entityName}"
			]

			unless model.isNew()
				breadcrumbsItems.push 
					icon: @icon
					name: model.getBreadcrumbName()
					url: "/#{@entityName}/#{model.id}/view"

			breadcrumbsItems.push 
				icon: if model.isNew() then "plus" else "edit"
				name: if model.isNew() then "common:actions.add" else "common:actions.edit"
				active: true

			breadcrumbs = new BreadcrumbsView
				items: breadcrumbsItems

			panel = new PanelLayout
				title: @formTitle

			form = new @formView
				model: model

			Config.App.getLayout().content.currentView.breadcrumbs.show breadcrumbs
			Config.App.getLayout().content.currentView.content.show panel
			panel.content.show form