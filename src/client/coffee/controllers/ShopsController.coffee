define [
	"controllers/BaseEntitiesController"
	"config"

	"views/BreadcrumbsView"

	"layouts/TabLayout"

	"models/ShopModel"
	"views/entities/shops/FormView"
	"views/entities/shops/TableView"
	"views/entities/shops/SingleView"
	"views/entities/shops/GraphView"

	"collections/ShopPricesCollection"

], (BaseEntitiesController, Config
	BreadcrumbsView
	TabLayout 
	ShopModel
	FormView, TableView, SingleView, GraphView
	ShopPricesCollection)->

	class ShopsController extends BaseEntitiesController

		listTitle: "common:entities.shops"

		formTitle: "common:entities.shop"

		model: ShopModel

		listView: TableView

		formView: FormView

		singleView: SingleView

		entityName: "shops"

		icons: "shopping-cart"

		icon: "shopping-cart"

		showEntity:(model, tab)->
			breadcrumbsItems = [
				icon: "dashboard"
				name: "common:dashboard"
				url: "/"
			,
				icon: @icons
				name: @listTitle
				url: "/#{@entityName}"
			,
				icon: @icon
				name: model.getBreadcrumbName()
				translate: no
				active: true
			]

			breadcrumbs = new BreadcrumbsView
				items: breadcrumbsItems

			generalInfoView = new @singleView
				model: model

			shopPricesCollection = new ShopPricesCollection

			shopPricesCollection.url = "/api/shops/#{model.id}/prices"

			graphView = new GraphView
				collection: shopPricesCollection
				model: model

			#tableView = new SummaryTableView
			#tableView.url = "/api/prices/good/#{model.id}"

			tabs = new TabLayout
				activeTab: tab
				tabs: [
					name: "graph"
					title: "common:graph"
					icon: "line-chart"
				,
					name: "general"
					title: "common:general"
					icon: "info"
				]

			Config.App.getLayout().content.currentView.breadcrumbs.show breadcrumbs

			Config.App.getLayout().content.currentView.content.show tabs

			tabs.general.show generalInfoView

			tabs.graph.show graphView

			#tabs.table.show tableView