define [
	"controllers/BaseHierarchyEntityController"
	"config"
	"models/RoleModel"
	"collections/RolesCollection"
	"views/entities/roles/FormView"
	"views/entities/roles/TreeView"
	"views/entities/roles/SingleView"
], (BaseHierarchyEntityController, Config
	RoleModel
	RolesCollection
	FormView, TreeView, SingleView)->

	class RolesController extends BaseHierarchyEntityController

		listTitle: "common:entities.roles"

		formTitle: "common:entities.role"

		model: RoleModel

		collection: RolesCollection

		listView: TreeView

		formView: FormView

		singleView: SingleView

		entityName: "roles"

		icons: "sitemap"

		icon: "sitemap"