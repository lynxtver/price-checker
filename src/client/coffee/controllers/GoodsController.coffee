define [
	"controllers/BaseEntitiesController"
	"config"

	"views/BreadcrumbsView"

	"layouts/TabLayout"

	"models/GoodModel"
	"views/entities/goods/FormView"
	"views/entities/goods/TableView"
	"views/entities/goods/SingleView"
	"views/entities/goods/GraphView"
	"views/entities/goods/SummaryTableView"

	"collections/GoodPricesCollection"
], (BaseEntitiesController, Config
	BreadcrumbsView
	TabLayout
	GoodModel
	FormView, TableView, SingleView, GraphView, SummaryTableView
	GoodPricesCollection)->

	class ShopsController extends BaseEntitiesController

		listTitle: "common:entities.goods"

		formTitle: "common:entities.good"

		model: GoodModel

		listView: TableView

		formView: FormView

		singleView: SingleView

		entityName: "goods"

		icons: "tags"

		icon: "tag"

		showEntity:(model, tab)->
			breadcrumbsItems = [
				icon: "dashboard"
				name: "common:dashboard"
				url: "/"
			,
				icon: @icons
				name: @listTitle
				url: "/#{@entityName}"
			,
				icon: @icon
				name: model.getBreadcrumbName()
				translate: no
				active: true
			]

			breadcrumbs = new BreadcrumbsView
				items: breadcrumbsItems

			generalInfoView = new @singleView
				model: model

			goodPricesCollection = new GoodPricesCollection

			goodPricesCollection.url = "/api/goods/#{model.id}/prices"

			graphView = new GraphView
				collection: goodPricesCollection
				model: model

			tableView = new SummaryTableView
			tableView.url = "/api/prices/good/#{model.id}"

			tabs = new TabLayout
				activeTab: tab
				tabs: [
					name: "graph"
					title: "common:graph"
					icon: "line-chart"
				,
					name: "general"
					title: "common:general"
					icon: "info"
				,
					name: "table"
					title: "common:table"
					icon: "table"
				]

			Config.App.getLayout().content.currentView.breadcrumbs.show breadcrumbs

			Config.App.getLayout().content.currentView.content.show tabs

			tabs.general.show generalInfoView

			tabs.graph.show graphView

			tabs.table.show tableView