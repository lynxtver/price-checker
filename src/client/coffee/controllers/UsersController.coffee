define [
	"controllers/BaseEntitiesController"
	"config"
	"models/UserModel"
	"views/entities/users/FormView"
	"views/entities/users/TableView"
	"views/entities/users/SingleView"
], (BaseEntitiesController, Config, 
	UserModel
	FormView, TableView, SingleView)->

	class UsersController extends BaseEntitiesController

		listTitle: "common:entities.users"

		formTitle: "common:entities.user"

		model: UserModel

		listView: TableView

		formView: FormView

		singleView: SingleView

		entityName: "users"

		icons: "users"

		icon: "user"