define [
	"controllers/BaseController"
	"config"
	"layouts/GridLayout"
	"layouts/PanelLayout"
	"views/BreadcrumbsView"
	"views/entities/prices/SummaryByShopsTableView"

	"collections/GoodsCollection"
], (BaseController, Config, GridLayout, PanelLayout, BreadcrumbsView, SummaryTableView, GoodsCollection)->

	class SummaryByShopsController extends BaseController

		summary:->
			do @setupLayout

			breadcrumbs = new BreadcrumbsView
				items: [
					icon: "dashboard"
					name: "common:dashboard"
					url: "/"
				,
					icon: "book"
					name: "common:summary"
					active: true
				]

			panel = new PanelLayout
				title: "common:summary"

			Config.App.getLayout().content.currentView.breadcrumbs.show breadcrumbs
			Config.App.getLayout().content.currentView.content.show panel

			goodCollection = new GoodsCollection

			goodCollection.fetch().done ->
				summaryTableView = new SummaryTableView
					goods: goodCollection
				panel.content.show summaryTableView

		setupLayout:->
			grid = new GridLayout
				rows: [
					columns: [
						name: "breadcrumbs"
					]
				,
					columns: [
						name: "content"
					]
				]
			do Config.App.setDefaultLayout
			
			Config.App.getLayout().content.show grid