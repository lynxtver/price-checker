define [
	"controllers/BaseEntitiesController"
	"config"
	"models/TaskModel"
	"views/entities/tasks/TableView"
], (BaseEntitiesController, Config, 
	TaskModel
	TableView)->

	class TasksController extends BaseEntitiesController

		listTitle: "common:entities.tasks"

		formTitle: "common:entities.task"

		model: TaskModel

		listView: TableView

		entityName: "tasks"

		icons: "tasks"

		icon: "tasks"