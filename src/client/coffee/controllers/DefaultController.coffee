define [
	"controllers/BaseController"
	"config"
], (BaseController, Config)->

	class DefaultController extends BaseController

		pageNotFound:->
			do Config.App.getLayout().content.empty