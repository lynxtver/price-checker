define [
	"controllers/BaseController"
	"config"
	"layouts/GridLayout"
	"views/widgets/ClockWidget"
], (BaseController, Config, GridLayout, ClockWidget)->

	class IndexController extends BaseController

		dashboard:->
			do Config.App.setDefaultLayout

			grid = new GridLayout
				rows: [
					columns: [
						size: "col-lg-4 col-sm-4"
					,
						size: "col-lg-4 col-sm-4"
					,
						size: "col-lg-4 col-sm-4"
						name: "clock"
					]
				]

			clockWidget = new ClockWidget

			Config.App.getLayout().content.show grid
			grid.clock.show clockWidget