define [
	"controllers/BaseEntitiesController"
	"config"
	"models/PriceModel"
	"views/entities/prices/FormView"
	"views/entities/prices/TableView"
	"views/entities/prices/SingleView"
], (BaseEntitiesController, Config, 
	PriceModel
	FormView, TableView, SingleView)->

	class PricesController extends BaseEntitiesController

		listTitle: "common:entities.prices"

		formTitle: "common:entities.price"

		model: PriceModel

		listView: TableView

		formView: FormView

		singleView: SingleView

		entityName: "prices"

		icons: "money"

		icon: "money"