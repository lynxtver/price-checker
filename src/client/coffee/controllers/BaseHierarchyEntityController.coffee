define [
	"controllers/BaseEntitiesController"
	"config"
	"layouts/PanelLayout"
	"views/BreadcrumbsView"
], (BaseEntitiesController, Config, PanelLayout, BreadcrumbsView)->

	class BaseHierarchyEntityController extends BaseEntitiesController

		list:->
			itemsCollection = new @collection
			entitiesView = new @view
				entityName: @entityName
			entitiesListView = new @listView
				collection: itemsCollection
			
			panel = new PanelLayout
				title: @listTitle

			do @setupLayout

			breadcrumbs = new BreadcrumbsView
				items: [
					icon: "dashboard"
					name: "common:dashboard"
					url: "/"
				,
					icon: @icons
					name: @listTitle
					active: true
				]

			itemsCollection.fetch()
				.done ->
					Config.App.getLayout().content.currentView.breadcrumbs.show breadcrumbs
					Config.App.getLayout().content.currentView.content.show panel
					panel.content.show entitiesView
					entitiesView.list.show entitiesListView