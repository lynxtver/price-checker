define [
	"config"
], (Config)->

	userName:->
		user = Config.App.Session.getUser()
		if user 
			name = user.name
			login = user.login
			name ? login

	checkPermissions:(permissions)->
		Config.App.Session.checkPermissions permissions

	isAdmin:->
		Config.App.Session.isAdmin()