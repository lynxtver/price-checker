define [
	"marionette"
], (Marionette)->

	class ModalsRegion extends Marionette.Region

		el: "#modals-container"

		show:(view, options)->
			if @currentView?
				self = @
				@listenToOnce @currentView, "modal:hidden", ->
					_.defer ->
						self.show view, options
			else
				return super view, options

			@