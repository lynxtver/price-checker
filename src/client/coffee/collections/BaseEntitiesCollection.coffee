define [
	"backbone"
], (Backbone)->

	class BaseEntitiesCollection extends Backbone.Collection

		parse:(response)->
			response.data ? []