define [
	"collections/BaseEntitiesCollection"
], (BaseEntitiesCollection)->

	class ShopsCollection extends BaseEntitiesCollection

		url: "/api/shops"