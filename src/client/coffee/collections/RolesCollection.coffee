define [
	"collections/BaseEntitiesCollection"
], (BaseEntitiesCollection)->

	class RolesCollection extends BaseEntitiesCollection

		url: "/api/roles"