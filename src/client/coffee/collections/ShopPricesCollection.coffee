define [
	"collections/BaseEntitiesCollection"
], (BaseEntitiesCollection)->

	class ShopPricesCollection extends BaseEntitiesCollection

		url: "/api/shops/"