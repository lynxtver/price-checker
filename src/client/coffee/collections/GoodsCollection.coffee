define [
	"collections/BaseEntitiesCollection"
], (BaseEntitiesCollection)->

	class GoodsCollection extends BaseEntitiesCollection

		url: "/api/goods"