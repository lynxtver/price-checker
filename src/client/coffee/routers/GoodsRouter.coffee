define [
	"routers/BaseEntitiesRouter"
], (BaseEntitiesRouter)->
	
	class GoodsRouter extends BaseEntitiesRouter

		initialize:->
			super

			@appRoute "#{@entityName}/:id/view/:tab(/)", 
				permissions: "#{@entityName}.view"
				callback: "viewEntity"