define [
	"routers/AuthenticatedRouter"
], (AuthenticatedRouter)->
	
	class SummaryByShopsRouter extends AuthenticatedRouter
		
		menuId: "summaryByShops"

		appRoutes:
			"summary/shops(/)": "summary"