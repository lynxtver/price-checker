define [
	"routers/BaseRouter"
	"config"
], (BaseRouter, Config)->

	class AuthenticatedRouter extends BaseRouter

		auth: true

		onBeforeRoute:(permissions, args)->
			if @getOption "auth"
				deferred = new $.Deferred
				Config.App.Session.getAuth()
					.always ->
						unless Config.App.Session.getUser()
							do deferred.reject
							Backbone.history.navigate "/login", true
						else
							unless Config.App.Session.checkPermissions permissions
								_.defer ->
									modal = Config.App.showWarningMsg "error:common.sufficientPrivileges"
									modal.promise().always ->
										do window.history.back
								do deferred.reject
							else
								do deferred.resolve

				do deferred.promise