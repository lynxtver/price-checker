define [
	"routers/AuthenticatedRouter"
], (AuthenticatedRouter)->
	
	class BaseEntitiesRouter extends AuthenticatedRouter

		auth: true

		constructor:(@options = {})->
			controller = @getOption "controller"
			@entityName = controller.entityName
			super

		initialize:->
			@appRoute "#{@entityName}(/)", 
				permissions: "#{@entityName}.view"
				callback: "list" 
			@appRoute "#{@entityName}/add(/)", 
				permissions: "#{@entityName}.add"
				callback: "addEdit"
			@appRoute "#{@entityName}/:id/edit(/)", 
				permissions: "#{@entityName}.edit"
				callback: "addEdit"
			@appRoute "#{@entityName}/:id/view(/)", 
				permissions: "#{@entityName}.view"
				callback: "viewEntity"

		getMenuId:->
			@entityName