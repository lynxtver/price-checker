define [
	"routers/BaseRouter"
], (BaseRouter)->
	
	class SessionRouter extends BaseRouter
		
		appRoutes:
			"login(/)": "login"
			"logout(/)": "logout"