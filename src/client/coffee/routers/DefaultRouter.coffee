define [
	"marionette"
], (Marionette)->
	
	class DefaultRouter extends Marionette.AppRouter
		
		appRoutes:
			"*path": "pageNotFound"