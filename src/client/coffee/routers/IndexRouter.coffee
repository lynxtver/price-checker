define [
	"routers/AuthenticatedRouter"
], (AuthenticatedRouter)->
	
	class IndexRouter extends AuthenticatedRouter
		
		menuId: "index"

		appRoutes:
			"(/)": "dashboard"