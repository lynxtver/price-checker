define [
	"marionette"
], (Marionette)->
	
	class BaseRouter extends Marionette.AppRouter

		getMenuId:-> 
			@getOption "menuId"

		appRoute:(route, methodName)->
			controller = @_getController()
			method = controller[methodName.callback ? methodName]
			@route route, methodName, _.bind method, controller

		route: (route, name, callback)->
			route = @_routeToRegExp route if not _.isRegExp route
			if _.isFunction name
				callback = name
				name = ""

			callback = @[name] unless callback
			router = @
			Backbone.history.route route, (fragment)->
				
				if _.isObject name
					routeName = name.callback
					permissions = name.permissions
				else
					routeName = name
					permissions = []				

				args = router._extractParameters route, fragment
				beforeRouteMethod = Marionette.getOption router, "onBeforeRoute"
				if _.isFunction beforeRouteMethod
					beforeRoute = beforeRouteMethod.apply router, [permissions].concat [args]
					if beforeRoute?.done?
						beforeRoute
							.done ->
								router.execRoute routeName, callback, args
					else
						unless beforeRoute is false
							router.execRoute routeName, callback, args
				else
					router.execRoute routeName, callback, args
			@

		execRoute: (name, callback, args)->
			@execute callback, args
			@trigger.apply @, ["route:#{name}"].concat(args)
			@trigger 'route', name, args
			Backbone.history.trigger 'route', @, name, args