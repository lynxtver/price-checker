define [
	"routers/BaseEntitiesRouter"
], (BaseEntitiesRouter)->
	
	class ShopsRouter extends BaseEntitiesRouter

		initialize:->
			super

			@appRoute "#{@entityName}/:id/view/:tab(/)", 
				permissions: "#{@entityName}.view"
				callback: "viewEntity"