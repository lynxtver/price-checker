define [
	"routers/AuthenticatedRouter"
], (AuthenticatedRouter)->
	
	class SummaryRouter extends AuthenticatedRouter
		
		menuId: "summaryByGoods"

		appRoutes:
			"summary/goods(/)": "summary"