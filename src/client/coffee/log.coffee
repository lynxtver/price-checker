define [
	"log4javascript"
	"config"
], (log4javascript, Config)->
	logger = log4javascript.getLogger()
	appender = new log4javascript.BrowserConsoleAppender()
	layout = new log4javascript.PatternLayout "%d{HH:mm:ss,SSS} [%-5p] %m"
	appender.setLayout layout
	logger.addAppender appender
	Config.Logger = logger
	logger.setLevel log4javascript.Level[Config.LogLevel]
	logger