define [
	"backbone"
], (Backbone)->

	class LinkModel extends Backbone.Model

		validation:
			shop:
				required: true
				msg: "error:common.required"
			url:
				required: true
				msg: "error:common.required"