define [
	"backbone"
], (Backbone)->

	class BaseEntityModel extends Backbone.Model

		getBreadcrumbName:->
			@id