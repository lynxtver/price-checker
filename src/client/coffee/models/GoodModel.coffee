define [
	"models/BaseEntityModel"
], (BaseEntityModel)->

	class GoodModel extends BaseEntityModel

		urlRoot: "/api/goods"

		validation:
			name:
				required: true
				msg: "error:common.required"

		getBreadcrumbName:->
			name = @get "name"
			"#{name}"