define [
	"models/BaseEntityModel"
], (BaseEntityModel)->

	class UserModel extends BaseEntityModel

		urlRoot: "/api/users"

		validation:
			email: [
				required: true
				msg: "error:common.required"
			,
				pattern: 'email'
				msg: "error:common.email"
			]

			name:
				required: true
				msg: "error:common.required"

			login:
				required: true
				msg: "error:common.required"

			password:
				required: (value, attr, computedState)->
					@isNew()
				msg: "error:common.required"

			passwordConfirm: [
				required: (value, attr, computedState)->
					@isNew() or not _.isEmpty computedState.password
				msg: "error:common.required"
			,
				fn: (value, attr, computedState)->
					unless computedState.password is value
						"error:users.passwordsNotMatch"
			]

		asyncValidation:
			email: 
				fn: (deferred, model, attr, value)->
					data = {}
					data[attr] = value
					model.save attr, value, 
						url: "#{model.url()}/validate"
						attrs: data
						wait: true
						validate: false
					.done (res)->
						do deferred.resolve
					.fail ->
						do deferred.reject
				msg: "error:users.existsWithEmail"

			login: 
				fn: (deferred, model, attr, value)->
					data = {}
					data[attr] = value
					model.save attr, value,
						url: "#{model.url()}/validate"
						attrs: data
						wait: true
						validate: false
					.done (res)->
						do deferred.resolve
					.fail ->
						do deferred.reject
				msg: "error:users.existsWithLogin"

		getBreadcrumbName:->
			name = @get "name"
			"#{name}"