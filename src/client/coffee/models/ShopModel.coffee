define [
	"models/BaseEntityModel"
], (BaseEntityModel)->

	class ShopModel extends BaseEntityModel

		urlRoot: "/api/shops"

		validation:
			name:
				required: true
				msg: "error:common.required"
			selector:
				required: true
				msg: "error:common.required"

		getBreadcrumbName:->
			name = @get "name"
			"#{name}"