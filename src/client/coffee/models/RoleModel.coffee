define [
	"models/BaseEntityModel"
], (BaseEntityModel)->

	class RoleModel extends BaseEntityModel

		urlRoot: "/api/roles"

		validation:
			name: [
				required: true
				msg: "error:common.required"
			,
				pattern: /^[a-z][a-z0-9._-]+$/
				msg: "error:common.roles.format"
			]

		getBreadcrumbName:->
			name = @get "name"
			"#{name}"