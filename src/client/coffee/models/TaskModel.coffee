define [
	"models/BaseEntityModel"
], (BaseEntityModel)->

	class TaskModel extends BaseEntityModel

		urlRoot: "/api/tasks"