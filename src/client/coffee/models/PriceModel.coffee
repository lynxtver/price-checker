define [
	"models/BaseEntityModel"
], (BaseEntityModel)->

	class PriceModel extends BaseEntityModel

		urlRoot: "/api/prices"

		validation:
			timestamp:
				required: true
				msg: "error:common.required"
			shop:
				required: true
				msg: "error:common.required"
			good:
				required: true
				msg: "error:common.required"
			price:
				required: true
				msg: "error:common.required"

		getBreadcrumbName:->
			good = @get "good"
			"#{good.name}"