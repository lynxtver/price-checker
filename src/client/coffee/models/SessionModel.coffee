define [
	"backbone"
	"config"
], (Backbone, Config)->

	class SessionModel extends Backbone.Model

		url: "/api/session"

		validation:
			login:
				required: true
				msg: "error:common.required"

			password:
				required: true
				msg: "error:common.required"

		initialize:->
			@listenTo @, "change:user", @onUserChange

		onUserChange:->
			user = @getUser()
			if user?
				Config.App.vent.trigger "session:login"
			else
				Config.App.vent.trigger "session:logout"

		getAuth:->
			do @fetch

		checkPermission:(checkedPermission)->
			result = _.some @getPermissions(), (permission)->
				permission.toLowerCase() is checkedPermission.toLowerCase()
			Config.Logger.debug "Checking permission [#{checkedPermission.toUpperCase()}] - ", if result then "GRANTED" else "DENIED"
			result

		checkPermissions:(checkedPermissions)->
			if _.isArray checkedPermissions
				_.every checkedPermissions, (checkedPermission)->
					@checkPermission checkedPermission
				, @
			else
				@checkPermission checkedPermissions

		getPermissions:->
			@get("permissions") ? []

		getUser:->
			@get "user"

		isAdmin:->
			user = @get "user"
			user?.admin

		getToken:->
			@get "token"

		sync:->
			self = @
			super
				.done (response, responseText, jqXHR)->
					self.unset "login"
					unless response.user
						self.set "user", null
				.fail ->
					self.set "user", null
				.always ->
					self.unset "password"