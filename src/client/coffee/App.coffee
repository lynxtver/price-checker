define [
	"backbone"
	"marionette"
	"i18n!locales"
	"config"
	"components/moment"
	"components/numeral"
	"templateHelpers"
	"regions/ModalsRegion"
	"routers/DefaultRouter"
	"controllers/DefaultController"
	"routers/IndexRouter"
	"controllers/IndexController"
	"routers/SummaryRouter"
	"controllers/SummaryController"
	"routers/SummaryByShopsRouter"
	"controllers/SummaryByShopsController"
	"routers/SessionRouter"
	"controllers/SessionController"
	"routers/BaseEntitiesRouter"
	"routers/ShopsRouter"
	"controllers/ShopsController"
	"routers/GoodsRouter"
	"controllers/GoodsController"
	"controllers/TasksController"
	"controllers/PricesController"
	"layouts/DefaultLayout"
	"layouts/ErrorModalLayout"
	"layouts/InformationModalLayout"
	"layouts/WarningModalLayout"
	"views/MenuView"
	"views/TopMenuView"
	"views/LanguageSelectorView"
	"views/SessionMenuView"
	"models/SessionModel"
], (Backbone, Marionette, i18n, Config, moment, numeral, templateHelpers
	ModalsRegion
	DefaultRouter, DefaultController
	IndexRouter, IndexController
	SummaryRouter, SummaryController
	SummaryByShopsRouter, SummaryByShopsController
	SessionRouter, SessionController
	BaseEntitiesRouter
	ShopsRouter, ShopsController
	GoodsRouter, GoodsController
	TasksController
	PricesController
	DefaultLayout, ErrorModalLayout, InformationModalLayout, WarningModalLayout
	MenuView, TopMenuView, LanguageSelectorView, SessionMenuView
	SessionModel)->

	Marionette.Behaviors.behaviorsLookup = ->
		Config.Behaviors

	renderer = Marionette.Renderer.render

	Marionette.Renderer.render = (template, data = {})->
		
		data = 
			data: data
			_locals: {}

		data.i18n = i18n
		data.moment = moment
		data.helpers = templateHelpers
		data.numeral = numeral
		data.Config = Config

		renderer template, data

	class Application extends Backbone.Marionette.Application

		setLayout:(Layout)->
			unless @main.currentView? and @main.currentView instanceof Layout
				layout = new Layout
				@main.show layout

		setDefaultLayout:->
			@setLayout DefaultLayout

		getLayout:->
			do @setDefaultLayout unless @main.currentView
			@main.currentView

		showModal:(modal)->
			@modals.show modal
			modal

		showError:(model, response)->
			data = response ? model
			if data?.responseJSON?
				message = data?.responseJSON?.error?.message
			else
				message = data?.error?.message
			@showErrorMsg message if message?

		showWarningMsg:(message)->
			warningModal = new WarningModalLayout
				title: "common:modal.warning.title"
				contentText: message
				autoClose: true

			@showModal warningModal

		showErrorMsg:(message)->
			errorModal = new ErrorModalLayout
				title: "common:modal.error.title"
				contentText: message
				autoClose: true

			@showModal errorModal

		showInfo:(response)->
			message = response?.message
			if message?
				infoModal = new InformationModalLayout
					title: "common:modal.info.title"
					contentText: message
					autoClose: true

				@showModal infoModal

		getMainMenu:->
			mainMenuCollection = new Backbone.Collection

			mainMenuCollection.add [
				id: "index"
				path: "/"
				icon: "dashboard"
				title: "common:dashboard"
			,
				id: "summaryByGoods"
				path: "/summary/goods"
				icon: "book"
				title: "common:summaryByGoods"
				permission: "prices.view"
			,
				id: "shops"
				path: "/shops/"
				icon: "shopping-cart"
				title: "common:entities.shops"
				permission: "shops.view"
			,
				id: "goods"
				path: "/goods/"
				icon: "tags"
				title: "common:entities.goods"
				permission: "goods.view"
			,
				id: "prices"
				path: "/prices/"
				icon: "money"
				title: "common:entities.prices"
				permission: "prices.view"
			,
				id: "tasks"
				path: "/tasks/"
				icon: "tasks"
				title: "common:entities.tasks"
				permission: "tasks.view"
			]

			new MenuView
				collection: mainMenuCollection

		getTopMenu:->
			topMenuCollection = new Backbone.Collection

			topMenuCollection.add [
				view: LanguageSelectorView
			,
				view: SessionMenuView
			]

			@topMenu = new TopMenuView
				collection: topMenuCollection

	App = new Application

	App.Session = new SessionModel

	App.Services = {}

	App.addInitializer ->
		
		App.addRegions
			main: "main"
			modals: ModalsRegion

		$(document).on "click", "a.disabled", (event)->
			do event.preventDefault

		$(document).on "click", "a[href^='/']:not(.ignore):not(.disabled)", (event)->
			$link = $(event.currentTarget)
			href = $link.attr "href"
			Backbone.history.navigate href, true
			do event.preventDefault

		$(document).on "click", "a.back", (event)->
			do event.preventDefault
			do window.history.back

		new DefaultRouter
			controller: new DefaultController

		new SessionRouter
			controller: new SessionController

		new IndexRouter
			controller: new IndexController

		new SummaryRouter
			controller: new SummaryController

		###
		new SummaryByShopsRouter
			controller: new SummaryByShopsController
		###

		new ShopsRouter
			controller: new ShopsController

		new GoodsRouter
			controller: new GoodsController

		new BaseEntitiesRouter
			controller: new TasksController	

		new BaseEntitiesRouter
			controller: new PricesController	

	App.on "start", ->

		$("main").fadeIn().removeClass "hide"
		$("#page-loader").fadeOut -> do $(@).remove

		Backbone.history.start
			pushState: true

	App