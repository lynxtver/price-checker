define [
	"marionette"
], (Marionette)->

	class BaseService extends Marionette.View

		destroy:->
			do @stopListening
			Marionette.triggerMethod.apply @, ["destroy"]