define [
	"marionette"
	"config"
	"i18n!locales:error"
	"components/AsyncValidation"
	"backbone-validation"
	"backbone.stickit"
], (Marionette, Config, i18n)->

	class ModelBindBehavior extends Marionette.Behavior

		onShow:->
			if @view.model
				Backbone.Validation.bind @view,
					forceUpdate: true
					valid: ((valid)->
						(view, attr)->
							unless view.model._asyncValidation?[attr]
								valid view, attr
					) @valid

					invalid: @invalid

				Backbone.AsyncValidation.bind @view,
					valid: @valid
					invalid: @invalid

				do @view.stickit
			
			@listenTo @view, "form:error", @invalid
			@listenTo @view, "form:ok", @valid

		onDestroy:->
			@stopListening @view, "form:error", @invalid
			@stopListening @view, "form:ok", @valid

		_getFormElement:(attr, selector)->
			$el = @view.$ "##{attr}"
			$el = @view.$ "[name='#{attr}']" if $el.length is 0
			$el = $(selector, $el) if selector
			$msg = $el.next("label")
			if $msg.length is 0
				$msg = $el.parent().next("label")
			if $msg.length is 0
				$msg = $("<label>")
					.addClass "error"
					.attr "for", "#{attr}"
				
				if $el.parent().is ".input-group"
					$msg.insertAfter $el.parent()
				else
					$msg.insertAfter $el
			$el: $el
			$msg: $msg

		valid:(view, attr)=>
			@hideError view, attr

		hideError:(view, attr)->
			data = @_getFormElement(attr)
			data.$el.removeClass "error"
			data.$msg.remove()

		showError:(view, attr, error)->

			if _.isObject(error) and error.index?
				data = @_getFormElement(attr, "[name='#{error.attr}']:eq(#{error.index})")
				data.$el.addClass "error"
				data.$msg.text @_getErrorText error
			else
				data = @_getFormElement(attr)
				data.$el.addClass "error"
				data.$msg.text @_getErrorText error

		_getErrorText:(error)->
			if _.isObject error
				i18n.t error.msg, error.options
			else
				i18n.t error

		invalid:(view, attr, error)=>
			return if @stickitChange? and @stickitChange.observe isnt attr
			self = @
			if _.isArray error
				@hideError view, attr
				_.each error, (item)->
					self.showError view, attr, item
			else
				@showError view, attr, error

	Config.Behaviors.ModelBind = ModelBindBehavior