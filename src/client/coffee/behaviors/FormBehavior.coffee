define [
	"marionette"
	"config"
	"i18n!locales:error"
	"components/AsyncValidation"
	"backbone-validation"
	"backbone.stickit"
	"backbone.babysitter"
	"jquery.form"
], (Marionette, Config, i18n)->

	class FormBehavior extends Marionette.Behavior

		defaults:
			saveMethod: "save"

		events:
			"click [type='submit']": "onFormSubmit"
			"keypress": "onKeyPress"

		initialize:->
			@view.components = new Backbone.ChildViewContainer

		onBeforeDestroy:->
			@view.components.call "destroy"

		onKeyPress:(e)->
			if e?.which is 13
				unless $(e.target).is "textarea"
					do @onFormSubmit 

		filesExists:->
			@view.$("input[type='file']").length isnt 0

		saveModel:->
			@view.triggerMethod "before:form:save"
			unless @filesExists()
				@view.model[@options.saveMethod](null, validate: false)
					.done @_saveDoneCallback
					.fail @_saveFailCallback
					.always @_saveAlwaysCallback
			else
				@view.$el.ajaxSubmit
					semantic: true
					data: @view.model.toJSON()
					url: @view.model.url()
					type: if @view.model.isNew() then "POST" else "PUT"
					success: @_saveDoneCallback
					error: @_saveFailCallback
					complete: @_saveAlwaysCallback

		_saveAlwaysCallback:(response, statusText, jqXHR)=>
			@view.triggerMethod "after:form:save", response, statusText, jqXHR

		_saveDoneCallback:(response, statusText, jqXHR)=>
			if response.error
				@view.triggerMethod "form:fail", response, statusText, jqXHR
			else
				@view.triggerMethod "form:done", response, statusText, jqXHR

		_saveFailCallback:(jqXHR, statusText, error)=>
			if jqXHR.status is 400
				if jqXHR.responseJSON?.error?.errors
					_.each jqXHR.responseJSON.error.errors, (field, attr)=>
						if field.path?
							@view.trigger "form:error", @view, field.path, field.message
						else
							@view.trigger "form:error", @view, attr, field.message

				if jqXHR.responseJSON?.error?.formErrors
					@showFormError jqXHR.responseJSON.error.formErrors
			@view.triggerMethod "form:fail", jqXHR.responseJSON, statusText, jqXHR

		onAfterFormSave:->
			@view.$(".form-buttons button, .form-buttons a").removeClass "disabled"

		onBeforeFormSave:->
			do @hideFormError
			@view.$(".form-buttons button, .form-buttons a").addClass "disabled"

		showFormError:(errors)->
			errorMsg = errors
			if _.isArray errors
				errorMsg = _.reduce errors, (memo, error)->
					memo += i18n.t(error)
				, ""
			else
				errorMsg = i18n.t errors
			
			@view.$(".form-error").text errorMsg

		hideFormError:->
		 	@view.$(".form-error").empty()

		onFormSubmit:->
			self = @
			@view.model.validate null, async: false
			if @view.model.isValid()
				unless @view.model.isAsyncValid?()
					@view.model.asyncValidate
						success:->
							do self.saveModel
				else
					do @saveModel

	Config.Behaviors.Form = FormBehavior