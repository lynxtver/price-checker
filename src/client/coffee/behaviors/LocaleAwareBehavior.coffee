define [
	"marionette"
	"config"
], (Marionette, Config, i18n)->

	class LocaleAwareBehavior extends Marionette.Behavior

		onShow:->
			@listenTo Config.App.vent, "locale:change", @_onLocaleChange

		_onLocaleChange:->
			@view.triggerMethod "locale:change"


	Config.Behaviors.LocaleAware = LocaleAwareBehavior