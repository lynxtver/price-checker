define [
	"marionette"
	"i18n!locales"
	"config"
], (Marionette, i18n, Config)->

	class ColumnLayout extends Marionette.LayoutView

		template: _.template ""

		initialize:->
			@size = @getOption("size") ? 12

		onRender:->
			@$el.addClass "col-lg-#{@size}"
			@addRegion "content", 
				selector: @$el
			@