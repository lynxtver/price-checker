define [
	"marionette"
	"config"
	"text!templates/layouts/default.html"
], (Marionette, Config, template)->

	class DefaultLayout extends Marionette.LayoutView

		tagName: "section"

		template: _.template template

		regions:
			content: "#main-content > section"
			leftSide: "#sidebar"
			topNav: "header > .top-nav"

		events:
			"click .sidebar-toggle-box .fa-bars": "toggleLeftSide"

		onShow:->
			@leftSide.show Config.App.getMainMenu()
			@topNav.show Config.App.getTopMenu()

		getContent:->
			@content

		toggleLeftSide:(e)->
			@leftSide.$el.toggleClass "hide-left-bar"
			@$('#main-content').toggleClass "merge-left"
			do e.stopPropagation