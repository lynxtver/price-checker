define [
	"layouts/ModalLayout"
	"text!templates/layouts/errorModal.html"
], (ModalLayout, template)->

	class ErrorModalLayout extends ModalLayout

		autoClose: true

		template: _.template template