define [
	"marionette"
	"config"
	"text!templates/layouts/tab.html"
	"text!templates/layouts/tabLink.html"
], (Marionette, Config, template, tabLinkTemplate)->

	class TabLayout extends Marionette.LayoutView

		tagName: "section"

		attributes:
			class: "panel"

		template: _.template template

		tabLinkTemplate: _.template tabLinkTemplate

		ui:
			tabsLinks: "ul.nav.nav-tabs"
			tabsContent: ".tab-content"

		events:
			"show.bs.tab": "onTabShow"

		initialize:->
			@tabs = Marionette.getOption @, "tabs"

		onTabShow:(e)->
			$link = $(e.target)
			regionName = $link.data "regionName"
			region = @regionManager.get regionName
			region.$el.trigger "resize" if region

		onRender:->
			for tab, index in @tabs
				id = "#{@cid}_tab_#{tab.name ? index}"
				regionName = tab.name ? "tab_#{index}"

				$tabLink = $(Marionette.Renderer.render @tabLinkTemplate, _.extend {id: id, regionName: regionName}, tab)

				@ui.tabsLinks.append $tabLink
				
				tab.$tabLink = $tabLink
				
				$tab = $("<div>")
					.addClass "tab-pane"
					.attr "id", id

				@ui.tabsContent.append $tab

				tab.$tab = $tab

				tab.regionName = regionName
				@addRegion tab.regionName, "##{id}"
			@

			activeTab = @getOption "activeTab"

			if activeTab?
				tab = _.find @tabs, (tab)-> tab.name is activeTab 

			if activeTab and tab
				tab.$tabLink.addClass "active"
				tab.$tab.addClass "active"
			else
				@tabs[0].$tabLink.addClass "active"
				@tabs[0].$tab.addClass "active"
