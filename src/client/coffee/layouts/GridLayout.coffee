define [
	"marionette"
	"i18n!locales"
	"config"
], (Marionette, i18n, Config)->

	class GridLayout extends Marionette.LayoutView

		template: _.template ""

		parseSize:(size)->
			result = ""
			unless size?
				result = "col-lg-12"
			else if _.isFinite size
				size = 12 if size > 12
				size = 1 if size < 1
				result = "col-lg-#{size}"
			else if _.isString size
				result =  "#{size}"
			result

		parseOffset:(offset)->
			result = ""
			if _.isFinite offset
				offset = 12 if offset > 12
				offset = 1 if offset < 1
				result = "col-lg-offset-#{offset}"
			else if _.isString offset
				result =  "#{offset}"
			result

		onRender:->
			rows = @getOption("rows") ? []
			for row, i in rows
				rowId = "#{@cid}_#{row.name ? i}"
				$row = $("<div>")
					.addClass "row"
					.attr "id", rowId
				@$el.append $row

				for column, j in row.columns
					columnId = "#{rowId}_#{column.name ? j}"
					$column = $("<div>")
						.addClass @parseSize column.size
						.addClass @parseOffset column.offset
						.attr "id", columnId
					
					$row.append $column
					columnName = column.name ? "cell_#{i}_#{j}"
					@addRegion columnName,
						selector: $column
			@