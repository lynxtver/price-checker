define [
	"marionette"
	"config"
	"text!templates/layouts/panel.html"
], (Marionette, Config, template)->

	class PanelLayout extends Marionette.LayoutView

		tagName: "section"

		attributes:
			class: "panel"

		template: _.template template

		regions: 
			content: ".panel-body"

		events:
			"click header .tools a[data-dismiss='panel']": "onCloseClick"

		serializeData:->
			data = super
			data.title = @getOption("title") ? ""
			data.closable = @getOption("closable") ? false
			data

		onCloseClick:(e)->
			do e.preventDefault
			do @destroy if @getOption("closable")