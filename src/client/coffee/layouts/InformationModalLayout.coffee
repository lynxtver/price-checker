define [
	"layouts/ModalLayout"
	"text!templates/layouts/informationModal.html"
], (ModalLayout, template)->

	class InformationModalLayout extends ModalLayout

		autoClose: true

		template: _.template template