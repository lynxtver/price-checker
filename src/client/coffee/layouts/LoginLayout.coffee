define [
	"marionette"
	"config"
	"text!templates/layouts/login.html"
], (Marionette, Config, template)->

	class LoginLayout extends Marionette.LayoutView

		tagName: "section"

		template: _.template template

		regions:
			content: ".form-signin"
			topNav: "header > .top-nav"

		getContent:->
			@content

		onShow:->
			$("body").addClass "login-body"
			@topNav.show Config.App.getTopMenu()

		onDestroy:->
			$("body").removeClass "login-body"