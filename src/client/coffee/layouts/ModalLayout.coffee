define [
	"marionette"
	"config"
	"text!templates/layouts/modal.html"
], (Marionette, Config, template)->

	class ModalLayout extends Marionette.LayoutView

		DEFAULT_TITLE = "common:title"
		DEFAULT_CANCEL = "common:actions.cancel"
		DEFAULT_SUCCESS_TEXT = "common:actions.ok"
		DEFAULT_SUCCESS_COLOR = "success"

		autoClose: false

		attributes:
			class: "modal fade"

		template: _.template template

		regions: 
			content: ".modal-body"

		events:
			"hide.bs.modal": "modalHide"
			"hidden.bs.modal": "modalHidden"
			"click button.confirm": "modalConfirmed"

		initialize:->
			@deferred = new $.Deferred

			onConfirm = @getOption "onConfirm"
			@deferred.done onConfirm if _.isFunction onConfirm

			onCancel = @getOption "onCancel"
			@deferred.fail onCancel if _.isFunction onCancel
		
			@

		close:->
			@$el.modal 'hide'

		onShow:->
			do @$el.modal
			@$el.modal 'show'

		isAutoClose:->
			@getOption("autoClose") ? false

		modalConfirmed:->
			beforeConfirm = @getOption "onBeforeConfirm"
			if _.isFunction beforeConfirm
				unless beforeConfirm() is false
					@deferred.resolve @
					do @close if @isAutoClose()
			else
				@deferred.resolve @
				do @close if @isAutoClose()

		modalHide:(e)->
			@deferred.reject @ if e.currentTarget is e.target

		modalHidden:->
			@trigger "modal:hidden"
			do @destroy

		promise:->
			@deferred.promise()

		serializeData:->
			title: @getOption("title") ? DEFAULT_TITLE
			cancelText: @getOption("cancelText") ? DEFAULT_CANCEL
			successText: @getOption("successText") ? DEFAULT_SUCCESS_TEXT
			successColor: @getOption("successColor") ? DEFAULT_SUCCESS_COLOR
			content: @getOption("contentText")