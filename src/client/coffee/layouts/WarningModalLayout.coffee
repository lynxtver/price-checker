define [
	"layouts/ModalLayout"
	"text!templates/layouts/warningModal.html"
], (ModalLayout, template)->

	class WarningModalLayout extends ModalLayout

		autoClose: true

		template: _.template template