define [
	"marionette"
	"i18n!locales"
	"config"
], (Marionette, i18n, Config)->

	class RowsLayout extends Marionette.LayoutView

		template: _.template ""

		initialize:->
			@rows = Marionette.getOption @, "rows"

		onRender:->
			for row, index in @rows
				id = "#{@cid}_row_#{row.name ? index}"
				$row = $("<div>")
					.addClass "row"
					.attr "id", id
				@$el.append $row
				row.regionName = row.name ? "row_#{index}"
				@addRegion row.regionName, "##{id}"
			@

		onShow:->
			for row, index in @rows
				if row.content
					@["#{row.regionName}"].show row.content