requirejs.config
	baseUrl: "/js"
	paths: 
		underscore: "lib/underscore"
		marionette: "lib/backbone.marionette"
		backbone: "lib/backbone"
		"backbone.wreqr": "lib/backbone.wreqr"
		"backbone.babysitter": "lib/backbone.babysitter"
		jquery: "lib/jquery"
		"jquery-ui": "lib/jquery-ui.min"
		domReady: "lib/domReady"
		moment: "lib/moment"
		"moment.ru": "lib/moment.ru"
		"underscore.string": "lib/underscore.string"
		"backbone.stickit": "lib/backbone.stickit"
		"backbone.paginator": "lib/backbone.paginator"
		"backbone-validation": "lib/backbone-validation-amd"
		text: "lib/text"
		log4javascript: "lib/log4javascript"
		bootstrap: "lib/bootstrap"
		select2: "lib/select2"
		"i18next": "lib/i18next.amd"
		dropzone: "lib/dropzone-amd-module"
		datatables: "lib/jquery.dataTables"
		"datatables-bootstrap3": "lib/datatables"
		async: "lib/async"
		fileinput: "lib/fileinput"
		numeral: "lib/numeral"
		"numeral.ru": "lib/numeral.ru"
		"jquery.form": "lib/jquery.form"
		icheck: "lib/icheck"
		"bootstrap-datepicker": "lib/bootstrap-datepicker"
		"bootstrap-datepicker.ru": "lib/bootstrap-datepicker.ru"
		"bootstrap-datetimepicker": "lib/bootstrap-datetimepicker"
		"bootstrap-datetimepicker.ru": "lib/bootstrap-datetimepicker.ru"
		"highcharts": "lib/highcharts"
		"locales": "../locales"
		templates: "../templates"
	map:
		"*":
			i18n: "lib/i18next"
	shim:
		bootstrap:
			deps: ["jquery"]
		log4javascript:
			exports: "log4javascript"
		select2:
			deps: ["jquery"]
		"jquery-ui":
			deps: ["jquery"]
		"fullcalendar-langs":
			deps: ["fullcalendar"]
		noty: 
			deps: ["jquery"]
		icheck:
			deps: ["jquery"]
		"numeral.ru":
			deps: ["numeral"]
		fileinput: 
			deps: ["jquery"]
		"bootstrap-datepicker":
			deps: ["jquery"]
		"bootstrap-datepicker.ru":
			deps: ["bootstrap-datepicker"]
		"bootstrap-datetimepicker":
			deps: ["bootstrap"]
		"bootstrap-datetimepicker.ru":
			deps: ["bootstrap-datetimepicker"]
		"highcharts":
			deps: ["jquery"]
			exports: "Highcharts"
		
	i18next:
		ns: "common"
		fallbackLng: "en",
		detectLngQS: "locale",
		lowerCaseLng: true,
		useCookie: true,
		supportedLngs:
			en: ["common", "error", "datatables", "select2", "permissions"]
			ru: ["common", "error", "datatables", "select2", "permissions"]
		resGetPath: "__lng__/__ns__.json"

require [
	"jquery"
	"underscore"
	"underscore.string"
	"domReady"
	"App"
	"config"
	"vent"
	"log"
	"i18n!locales"
	"bootstrap"
], ($, _, _s, domReady, App, Config, Vent)->
	"use strict"

	_.str = _s

	_.mixin _.str.exports()

	$.ajaxSetup
		cache: false
		beforeSend: (xhr)->
			token = Config.App.Session.getToken()
			if token
				xhr.setRequestHeader 'X-CSRF-Token', token

	backboneSync = Backbone.sync

	Backbone.sync = (method, model, options)->
		errorCallback = options.error
		successCallback = options.success

		options.success = (response, status, options)->
			if response.error?
				Config.Logger.debug "Request [#{@url}] - ERROR: #{response.error.message}"
				successCallback.call @, {}, status, options
			else
				Config.Logger.debug "Request [#{@url}] - SUCCESS"
				successCallback.call @, response, status, options

		options.error = (jqXHR, statusText, error)->
			Config.Logger.error "Request [#{@url}] - ERROR: #{error}"
			errorCallback.apply @, Array::slice.apply arguments

		backboneSync method, model, options

	domReady ->
		window.App = Config.App = App
		App.vent = Vent.vent
		App.reqres = Vent.reqres
		App.commands = Vent.commands

		App.Session.once "change:user", ->
			do App.start

		do App.Session.getAuth