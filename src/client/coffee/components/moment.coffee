define [
	"i18next"
	"moment"
	"moment.ru"
], (i18n, moment)->
	->
		lng = i18n.lng().substring 0, 2
		moment.locale lng
		moment.apply null, arguments