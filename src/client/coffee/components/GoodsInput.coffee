define [
	"components/Select2View"
], (Select2View)->

	class GoodsInputView extends Select2View

		url: "/api/goods"

		formatResult:(item)->
			"<i class=\"fa fa-tag\"></i> #{item.name}"

		formatSelection:(item)->
			"<i class=\"fa fa-tag\"></i> #{item.name}"