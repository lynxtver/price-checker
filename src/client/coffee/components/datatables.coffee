define [
	"datatables-bootstrap3"
], ->

	$.extend true, $.fn.dataTable.defaults,
		dom: "<'row'<'col-xs-12'lf>>t<'row'<'col-sm-4'i><'col-sm-8'p>>r"
		pagingType: "bootstrap"
		classes:
			sFilterInput: "form-control search"
			sLengthSelect: "form-control"
		autoWidth: false

	$.fn.dataTableExt.errMode = "throw"

	`
	$.extend( $.fn.dataTableExt.oPagination, {
		"bootstrap": {
			"fnInit": function( oSettings, nPaging, fnDraw ) {
				var oLang = oSettings.oLanguage.oPaginate;
				var fnClickHandler = function ( e ) {
					e.preventDefault();
					if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
						fnDraw( oSettings );
					}
				};

				$(nPaging).addClass('pagination').append(
					'<ul>'+
						'<li class="first disabled"><a href="#"><i class="fa fa-angle-double-left"></i> '+oLang.sFirst+'</a></li>'+
						'<li class="prev disabled"><a href="#"><i class="fa fa-angle-left"></i> '+oLang.sPrevious+'</a></li>'+
						'<li class="next disabled"><a href="#">'+oLang.sNext+' <i class="fa fa-angle-right"></i></a></li>'+
						'<li class="last disabled"><a href="#">'+oLang.sLast+' <i class="fa fa-angle-double-right"></i></a></li>'+
					'</ul>'
				);
				var els = $('a', nPaging);
				$(els[0]).bind( 'click.DT', { action: "first" }, fnClickHandler );
				$(els[1]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
				$(els[2]).bind( 'click.DT', { action: "next" }, fnClickHandler );
				$(els[3]).bind( 'click.DT', { action: "last" }, fnClickHandler );
			},

			"fnUpdate": function ( oSettings, fnDraw ) {
				var iListLength = 5;
				var oPaging = oSettings.oInstance.fnPagingInfo();
				var an = oSettings.aanFeatures.p;
				var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);

				if ( oPaging.iTotalPages < iListLength) {
					iStart = 1;
					iEnd = oPaging.iTotalPages;
				}
				else if ( oPaging.iPage <= iHalf ) {
					iStart = 1;
					iEnd = iListLength;
				} else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
					iStart = oPaging.iTotalPages - iListLength + 1;
					iEnd = oPaging.iTotalPages;
				} else {
					iStart = oPaging.iPage - iHalf + 1;
					iEnd = iStart + iListLength - 1;
				}

				for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
					// Remove the middle elements
					$('li:gt(1)', an[i]).filter(':not(:last)').filter(':not(:last)').remove();

					// Add the new list items and their event handlers
					for ( j=iStart ; j<=iEnd ; j++ ) {
						sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
						$('<li '+sClass+'><a href="#">'+j+'</a></li>')
							.insertBefore( $('li:last', an[i]).prev()[0] )
							.bind('click', function (e) {
								e.preventDefault();
								oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
								fnDraw( oSettings );
							} );
					}

					// Add / remove disabled classes from the static elements
					if ( oPaging.iPage === 0 ) {
						$('li:first', an[i]).addClass('disabled');
						$('li:eq(1)', an[i]).addClass('disabled');
					} else {
						$('li:first', an[i]).removeClass('disabled');
						$('li:eq(1)', an[i]).removeClass('disabled');
					}

					if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
						$('li:last', an[i]).addClass('disabled');
						$('li:eq(-2)', an[i]).addClass('disabled');
					} else {
						$('li:last', an[i]).removeClass('disabled');
						$('li:eq(-2)', an[i]).removeClass('disabled');
					}

					if(oPaging.iTotalPages == 0 ) {
						$(an[i]).hide();
					} else {
						$(an[i]).show();
					}

					if(oPaging.iTotalPages > iListLength) {
						$('li:eq(0)', an[i]).show()
						$('li:eq(-1)', an[i]).show()
					} else {
						$('li:eq(0)', an[i]).hide()
						$('li:eq(-1)', an[i]).hide()
					}
				}
			}
		}
	} );
	`

	true