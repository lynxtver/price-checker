define [
	"components/Select2View"
], (Select2View)->

	class UsersInputView extends Select2View

		url: "/api/users"

		formatResult:(item)->
			"<i class=\"fa fa-user\"></i> #{item.name ? item.login}"

		formatSelection:(item)->
			"<i class=\"fa fa-user\"></i> #{item.name ? item.login}"

		queryData:=>
			data = super

			data.subordinate = true if @getOption "subordinate"

			data