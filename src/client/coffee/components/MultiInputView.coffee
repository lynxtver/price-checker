define [
	"marionette"
	"i18n!locales"
	"components/ControlView"
	"text!templates/components/multiinput.html"
	"text!templates/components/multiinputItem.html"
	"behaviors/ModelBindBehavior"
	"backbone.stickit"
], (Marionette, i18n, ControlView, template, itemTemplate, MultiInputItemView) ->

	class MultiInputItemModel extends Backbone.Model
		getView:-> @get "view"

	class MultiInputItemView extends Marionette.LayoutView

		attributes:
			class: "multiinput-item clearfix"

		template: _.template itemTemplate

		regions:
			content: ".multiinput-item-content"

		events:
			"click .btn-delete": "onDeleteClick"

		behaviors:
			ModelBind: {}

		getItemModel:->
			@_itemModel

		getItemView:->
			@_itemView

		onShow:->
			itemView = @model.getView()
			
			@_itemModel = itemView.model

			@_itemView = itemView

			@listenTo @_itemModel, "change", @onItemModelChange

			@content.show @_itemView

		onItemModelChange:(model)->
			@trigger "item:change", model

		onDestroy:->
			@stopListening @_itemModel, "change", @onItemModelChange

		onDeleteClick:(e)->
			do e.preventDefault
			do @model.destroy

	class MultiInputView extends Marionette.CompositeView

		template: _.template template

		ui:
			container: ".multiinput-items-container"
			toggle: "> .row button.toggle"
			toggleIcon: "> .row button.toggle i"
			info: "> .row .form-control-static.info"

		events:
			"click .addItem": "onAddItemClick"
			"click @ui.toggle": "onToggleClick"

		childViewContainer: ".multiinput-items-container"

		childView: MultiInputItemView

		constructor:->
			super
			do @render
			@trigger "show"

		initialize:->
			super
			@collection = new Backbone.Collection
			@$el.data "multiinput", @

		onRender:->
			do @ui.container.hide if @getOption "hideDefault"

		update:=>
			@ui.info.text i18n.t "info.items", count: @children.length
			unless @ui.container.is ":empty"
				@ui.toggle.removeClass "disabled"
				if @ui.container.is ":visible"
					@ui.toggleIcon.addClass "fa-chevron-up"
					@ui.toggleIcon.removeClass "fa-chevron-down"
				else
					@ui.toggleIcon.removeClass "fa-chevron-up"
					@ui.toggleIcon.addClass "fa-chevron-down"
			else
				do @ui.container.hide
				@ui.toggleIcon.removeClass "fa-chevron-up"
				@ui.toggleIcon.addClass "fa-chevron-down"
				@ui.toggle.addClass "disabled"

		serializeData:->
			data = super
			data.addButtonText = @getOption("addButtonText") ? 'common:actions.add'
			data

		onToggleClick:->
			if @ui.container.not ":empty"
				if @ui.container.is ":visible"
					@ui.container.slideUp
						complete: @update
				else
					@ui.container.slideDown
						complete: @update

		onAddItemClick:(e)->
			do e.preventDefault
			do @ui.container.show			
			do @createItem

		createItem:(modelData = {})->

			ItemModelClass = @getOption "itemModel"

			ItemViewClass = @getOption "itemView"

			itemModel = new ItemModelClass modelData

			itemView = new ItemViewClass
				model: itemModel

			item = new MultiInputItemModel
				view: itemView

			@collection.add item

		reset:->
			@collection.reset null

		onAddChild:(itemView)->
			@bindItemEvents itemView
			do @update
			@$el.trigger "multiinput:change"

		onRemoveChild:(itemView)->
			@unbindItemEvents itemView
			do @update
			@$el.trigger "multiinput:change"

		onItemModelChange:->
			@$el.trigger "multiinput:change"

		bindItemEvents:(item)->
			@listenTo item.getItemModel(), "change", @onItemModelChange

		unbindItemEvents:(item)->
			@stopListening item.getItemModel(), "change", @onItemModelChange

		getValue:->
			value = []
			allValid = @children.every (itemView)->
				model = itemView.getItemModel()
				if not _.isEmpty(model.attributes) and model.isValid true
					value.push model.toJSON()
					true
				else
					false

			if allValid then value else []

		@onGet: (value, options)->
			if value?
				value
			else
				[]
				
		@onSet: (value, options)->
			if value
				value
			else
				[]

		@getVal:($el, event, options)->
			view = $el.data "multiinput"
			do view.getValue

		@update:($el, val, model, options)->
			view = $el.data "multiinput"
			do view.reset
			_.each val, (item)->
				view.createItem item
			do view.update

	Backbone.Stickit.addHandler
		selector: ".multiinput"
		events: ["multiinput:change"]
		onGet: MultiInputView.onGet
		onSet: MultiInputView.onSet
		update: MultiInputView.update
		getVal: MultiInputView.getVal

	MultiInputView
