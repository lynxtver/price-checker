define [
	"components/ControlView"
	"icheck"
	"backbone.stickit"
], (ControlView, i18n) ->

	class CheckboxInputView extends ControlView

		initialize:(@options = {})->
			super

			@$el.iCheck
				checkboxClass: 'icheckbox_flat-grey'
				radioClass: 'iradio_flat-grey'

		onDestroy:->
			@$el.iCheck "destroy"

		@update:($el, val, model, options)->
			state = if val then "check" else "uncheck"
			$el.iCheck state

	Backbone.Stickit.addHandler
		selector: ".icheck"
		events: ["ifToggled"]
		update: CheckboxInputView.update

	CheckboxInputView