define [
	"i18next"
	"numeral"
	"numeral.ru"
], (i18n, numeral)->
	->
		lng = i18n.lng().substring 0, 2
		numeral.language lng
		numeral.apply null, arguments