define [
	"components/ControlView"
	"components/numeral"
	"backbone.stickit"
], (ControlView, numeral) ->

	class CurrencyInputView extends ControlView

		DATA_ATTR = "currencyValue"

		events:
			"ci.change": "onInput"
			"keydown": "onKeydown"
			"keyup": "onKeyup"

		onKeydown:(e)->
			val = @$el.data DATA_ATTR
			pass = false
			changed = false
			if 48 <= e.which <= 57
				val *= 10
				val += (e.which - 48) / 100
				pass = true
				changed = true

			else if e.which is 8
				val /= 10
				pass = true
				changed = true

			else if e.which in [9]
				pass = true

			unless pass
				e.preventDefault()
				e.stopPropagation()
			else
				if changed
					@$el.data DATA_ATTR, Number val.toFixed 2
					@$el.trigger "ci.change"

		onKeyup:(e)->
			do @format

		format:->
			val = @$el.data DATA_ATTR
			@$el.val numeral(val).format "0,0.00"					

		@onGet: (value, options)->
			if value
				value
			else
				""
				
		@onSet: (value, options)->
			if value
				value
			else
				null

		@getVal:($el, event, options)->
			$el.data DATA_ATTR

		@update:($el, val, model, options)->
			$el.data DATA_ATTR, val
			$el.val numeral(val).format "0,0.00"

	Backbone.Stickit.addHandler
		selector: ".currency-input"
		events: ["change", "ci.change"]
		onGet: CurrencyInputView.onGet
		onSet: CurrencyInputView.onSet
		update: CurrencyInputView.update
		getVal: CurrencyInputView.getVal

	CurrencyInputView