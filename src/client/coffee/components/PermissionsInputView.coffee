define [
	"components/Select2View"
	"i18n!locales:permissions"
], (Select2View, i18n)->

	class PermissionsInputView extends Select2View

		formatResult:(item)->
			"#{i18n.t "permissions:#{item.name.toLowerCase()}"} <span class=\"badge pull-right\">#{item.name}</span>"

		formatSelection:(item)->
			"#{i18n.t "permissions:#{item.name.toLowerCase()}"} <span class=\"badge\">#{item.name}</span>"