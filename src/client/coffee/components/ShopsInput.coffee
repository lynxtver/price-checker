define [
	"components/Select2View"
], (Select2View)->

	class ShopsInputView extends Select2View

		url: "/api/shops"

		formatResult:(item)->
			"<i class=\"fa fa-shopping-cart\"></i> #{item.name}"

		formatSelection:(item)->
			"<i class=\"fa fa-shopping-cart\"></i> #{item.name}"