define [
	"backbone"
	"backbone-validation"
], (Backbone)->

	class AsyncValidation 

		THROTTLE_DELAY = 300

		bind:(view, options)->
			model = view.model
			if model
				model._isAsyncValid = null
				model._asyncValidationCache = {}
				model._asyncValidation = {}
				model.asyncValidate = _.bind(@validate, model)
				model.isAsyncValid = _.bind ->
					@_isAsyncValid
				, model
				model.validate = do (model)->
					validate = model.validate
					asyncValidate = _.throttle model.asyncValidate, THROTTLE_DELAY, leading: false
					(attrs, options)->
						errors = validate.call model, attrs, options
						unless options?.async is false
							_.defer ->
								do asyncValidate
						errors

				for attr, cfg of model.asyncValidation
					callback = _.bind @callback, view, model, attr, _.extend({}, options, cfg)
					model._asyncValidation[attr] =
						callback: callback

		validate:(options = {})->
			deferreds = []
			self = @
			for attr, bind of @_asyncValidation
				deferreds.push bind.callback?()
			$.when.apply($, deferreds)
				.done ->
					self._isAsyncValid = true
					options.success?()
				.fail ->
					self._isAsyncValid = false
					options.error?()

		unbind:(view)->
			model = view.do
			if model
				for attr, bind of model._asyncValidation
					model.stopListening bind.callback

		callback:(model, attr, options)->
			unless model.isValid attr
				if model._asyncValidationCache[attr]?
					return model._asyncValidationCache[attr]
				else
					return true

			if not model.changed[attr]? and model._asyncValidationCache[attr]?
				return model._asyncValidationCache[attr]
	
			self = @

			deffered = $.Deferred()
			deffered
				.done ->
					model._asyncValidationCache[attr] = true
					options.valid self, attr
				.fail ->
					model._asyncValidationCache[attr] = false
					options.invalid self, attr, options.msg
			
			value = model.get attr
			options.fn(deffered, model, attr, value)
			deffered.promise()

	Backbone.AsyncValidation = new AsyncValidation