define [
	"async"
	"components/ControlView"
	"i18n!locales:select2"
	"select2"
	"backbone.stickit"
], (async, ControlView, i18n) ->

	class Select2InputView extends ControlView

		PER_PAGE = 10

		initialize:(@options = {})->
			super

			options =
				allowClear: @$el.data("allowClear") ? false
				formatSearching: @formatSearching
				formatNoMatches: @formatNoMatches
				formatLoadMore: @formatLoadMore

			@dataUrl = @getOption("url") ? @$el.data "url"

			@perPage = @getOption("perPage") ? PER_PAGE

			if @dataUrl?
				options.ajax =
					url: @dataUrl
					dataType: "json"
					data: @queryData
					results: @parseResults
				options.formatResult = @formatResult
				options.formatSelection = @formatSelection

			if @$el.is "input"
				options.initSelection = @initSelection
				options.multiple = @$el.data("multiple") ? false

			@$el.select2 options

		onDestroy:->
			@$el.select2 "destroy"

		parseResults:(response, page)=>
			results: response.data
			more: page * @perPage < response.recordsFiltered

		queryData:(term, page)=>
			data =
				search:
					value: term
				start: @perPage * (page - 1)
				length: @perPage

			_.extend data, _.result @, "queryParams"

		initSelection:(element, callback)=>
			self = @
			value = element.select2 "val"
			if _.isArray value
				async.map value, (item, cb)->
					if _.isObject item
						id = item.id
					else
						id = item
					$.get "#{self.dataUrl}/#{id}"
						.done (data)->
							cb null, data
				, (err, items)->
					callback items 
			else
				if _.isObject value
					id = value.id
				else
					id = value
				$.get "#{self.dataUrl}/#{id}"
					.done (data)->
						callback data

		formatResult:(item)->
			"#{item.name}"

		formatSelection:(item)->
			"#{item.name}"

		formatSearching:->
			i18n.t "select2:searching"

		formatNoMatches:->
			i18n.t "select2:nomatches"

		formatLoadMore:->
			i18n.t "select2:loadMore"

		getValue:->
			@$el.select2 "data"

		@onGet: (value, options)->
			if value
				if _.isArray value
					values = _.map value, (item)->
						if _.isObject item
							item.id
						else
							item
					values
				else
					if _.isObject value
						value.id
					else
						value
			else
				""
				
		@onSet: (value, options)->
			if value
				value
			else
				null

		@getVal:($el, event, options)->
			$el.select2 "val"

		@update:($el, val, model, options)->
			$el.select2 "val", val

	Backbone.Stickit.addHandler
		selector: ".select2"
		events: ["change"]
		onGet: Select2InputView.onGet
		onSet: Select2InputView.onSet
		update: Select2InputView.update
		getVal: Select2InputView.getVal

	Select2InputView