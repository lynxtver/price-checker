define [
	"components/ControlView"
	"components/moment"
	"i18next"
	"i18n!locales:common"
	"components/datepicker"
	"backbone.stickit"
], (ControlView, moment, i18next, i18n) ->

	class DateInputView extends ControlView

		initialize:->
			super

			format = "mm/dd/yyyy"

			lng = i18next.lng().substring(0, 2).toLowerCase()

			format = "dd.mm.yyyy" if lng is "ru"

			@$el.datepicker
				language: i18next.lng().substring 0, 2
				format: format

		onDestroy:->
			@$el.datepicker "remove"

		@onGet: (value, options)->
			result = null
			if value
				date = moment value
				
				if date.isValid()
					result = date.format i18n.t "common:locale.date"

			result
				
		@onSet: (value, options)->
			if value
				value
			else
				null

		@getVal:($el, event, options)->
			do $el.val

		@update:($el, val, model, options)->
			if val
				$el.datepicker "update", val
			else
				$el.val ""

	Backbone.Stickit.addHandler
		selector: ".datepicker"
		events: ["change"]
		onGet: DateInputView.onGet
		onSet: DateInputView.onSet
		update: DateInputView.update
		getVal: DateInputView.getVal

	DateInputView