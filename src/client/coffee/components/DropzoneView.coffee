define [
	"components/ControlView"
	"config"
	"dropzone"
], (ControlView, Config, Dropzone, template) ->

	Dropzone.autoDiscover = false

	class DropZoneView extends ControlView

		initialize:(@options = {})->
			super

			$zone = @$(".dropzone-zone").get 0
			$preview = @$(".dropzone-previews").get 0
			$clickable = @$(".dropzone-zone button").get 0

			@dropzone = new Dropzone $zone, _.extend {}, 
				previewsContainer: $preview
				clickable: $clickable
				url: @$el.data "url"
			, @options

			@listenTo @dropzone, "sending", @onSending
			@listenTo @dropzone, "success", @onSuccess
			@listenTo @dropzone, "error", @onError

		onDestroy:->
			do @dropzone.destroy

		onSending:(file, xhr, formData)=>
			token = Config.App.Session.getToken()
			if token
				xhr.setRequestHeader 'X-CSRF-Token', token

			data = @getOption "data"
			if data?
				for key, value of data
					formData.append key, value

		onSuccess:(file, response)=>
			file.id = response._id
			@trigger "success", file, response

		onError:(file, response, xhr)=>
			Config.App.showError file, response