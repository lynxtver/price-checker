define [
	"components/ControlView"
	"components/moment"
	"i18next"
	"i18n!locales:common"
	"components/datetimepicker"
	"backbone.stickit"
], (ControlView, moment, i18next, i18n) ->

	class DateTimeInputView extends ControlView

		initialize:->
			super

			format = "mm/dd/yyyy HH:ii"

			lng = i18next.lng().substring(0, 2).toLowerCase()

			format = "dd.mm.yyyy HH:ii" if lng is "ru"

			@$el.datetimepicker
				language: i18next.lng().substring 0, 2
				format: format
				fontAwesome: true
				autoclose: true

			@$el.data "view", @

			@

		onDestroy:->
			@$el.datetimepicker "remove"

		@onGet: (value, options)->
			result = null
			if value
				date = moment value
				
				if date.isValid()
					result = date

			result
				
		@onSet: (value, options)->
			if value
				value
			else
				null

		@getVal:($el, event, options)->
			moment($el.datetimepicker("getDate")).toISOString()

		@update:($el, val, model, options)->
			view = $el.data "view"
			value = val
			if value
				value = val.format i18n.t "common:locale.datetime"
			$el.val value
			$el.datetimepicker "update"

	Backbone.Stickit.addHandler
		selector: ".datetimepicker"
		events: ["change"]
		onGet: DateTimeInputView.onGet
		onSet: DateTimeInputView.onSet
		update: DateTimeInputView.update
		getVal: DateTimeInputView.getVal

	DateTimeInputView