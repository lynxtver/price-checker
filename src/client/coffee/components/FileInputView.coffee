define [
	"components/ControlView"
	"fileinput"
	"backbone.stickit"
], (ControlView) ->

	class FileInputView extends ControlView

		initialize:->
			super

			@$el.fileinput()

		@onGet: (value, options)->
			if value
				value
			else
				""
				
		@onSet: (value, options)->
			if value
				value
			else
				null

		@getVal:($el, event, options)->
			$el.find("input[type='file']").val()

		@update:($el, val, model, options)->
	
	Backbone.Stickit.addHandler
		selector: ".fileinput"
		events: ["change.bs.fileinput", "clear.bs.fileinput", "reset.bs.fileinput"]
		onGet: FileInputView.onGet
		onSet: FileInputView.onSet
		update: FileInputView.update
		getVal: FileInputView.getVal

	FileInputView