define [
	"backbone.wreqr"
], ->
	vent: new Backbone.Wreqr.EventAggregator
	reqres: new Backbone.Wreqr.RequestResponse
	commands: new Backbone.Wreqr.Commands