define [
	"marionette"
	"config"
	"text!templates/menu/top/sessionMenu/template.html"
	"behaviors/LocaleAwareBehavior"
], (Marionette, Config, template)->

	class SessionMenuView extends Marionette.ItemView

		tagName: "li"

		attributes:
			class: "dropdown language"

		template: _.template template

		behaviors:
			LocaleAware: {}

		initialize:->
			@listenTo Config.App.vent, "session:login", @render
			@listenTo Config.App.vent, "session:logout", @render

		onRender:->
			if Config.App.Session.getUser()
				do @$el.show
			else
				do @$el.hide

		onLocaleChange:->
			do @render