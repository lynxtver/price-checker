define [
	"views/entities/base/TableView"
	"components/moment"
	"i18n!locales:datatables,common"
], (TableView, moment, i18n)->

	class TasksTableView extends TableView

		url: "/api/tasks"

		entityName: "tasks"

		showRowNumbers: true

		showActions: false

		columns: [
			data: "started"
			title: "common:started"
			name: "started"
			searchable: true
			width: "50%"
			render: (data, type, row, meta)->
				moment(data).format i18n.t "common:locale.datetimefull"
		,
			data: "ended"
			title: "common:ended"
			name: "ended"
			searchable: true
			width: "50%"
			render: (data, type, row, meta)->
				if data?
					moment(data).format i18n.t "common:locale.datetimefull"
				else
					null
		,
			data: "grabbed"
			title: "common:grabbed"
			name: "grabbed"
			searchable: false
			orderable: false
			className: "text-center"
			defaultContent: ""
		,
			data: "errorsNumber"
			title: "common:errors"
			name: "errorsNumber"
			searchable: false
			orderable: false
			className: "text-center"
			defaultContent: ""
		]

		order: [1, 'desc']