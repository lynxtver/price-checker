define [
	"views/entities/base/TableView"
], (TableView)->

	class UsersTableView extends TableView

		url: "/api/users"

		entityName: "users"

		showRowNumbers: true

		showActions: true

		columns: [
			data: "email"
			title: "common:email"
			name: "email"
			searchable: true
			width: "50%"
		,
			data: "login"
			title: "common:login"
			name: "login"
			searchable: true
			width: "50%"
		]

		order: [1, 'asc']