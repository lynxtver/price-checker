define [
	"views/entities/base/SingleView"
	"config"
	"text!templates/entities/users/single.html"
], (SingleView, Config, template)->

	class UserView extends SingleView

		template: _.template template

		attributes:
			class: "form-horizontal"

		entityName: "users"