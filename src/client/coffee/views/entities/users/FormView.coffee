define [
	"views/entities/base/FormView"
	"text!templates/entities/users/form.html"
	"components/Select2View"
], (FormView, template, Select2View)->
	
	class UsersFormView extends FormView

		template: _.template template

		bindings:
			"#email": 
				observe: "email"
				setOptions:
					validate: true

			"#login": 
				observe: "login"
				setOptions:
					validate: true

			"#name": 
				observe: "name"
				setOptions:
					validate: true

			"#password": 
				observe: "password"
				setOptions:
					validate: true
				updateView: false

			"#passwordConfirm": 
				observe: "passwordConfirm"
				setOptions:
					validate: true
				updateView: false

			"#roles":
				observe: "roles"
				setOptions:
					validate: true

		onShow:->
			@components.add new Select2View
				el: "#roles"

		onFormDone:->
			Backbone.history.navigate "/users", true