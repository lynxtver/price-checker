define [
	"marionette"
	"config"
	"text!templates/entities/base/list.html"
], (Marionette, Config, template)->

	class BaseListView extends Marionette.LayoutView

		template: _.template template

		attributes:
			class: "entities"

		regions:
			list: ".list"

		initialize:->
			@entityName = @getOption "entityName"
			@parentEntityId = @getOption "parentEntityId"
			@parentEntity = @getOption "parentEntity"
			@readOnly = @getOption("readOnly") ? false

		serializeData:->
			data = super
			
			baseUrl = ""

			if @parentEntity and @parentEntityId
				baseUrl = "/#{@parentEntity}/#{@parentEntityId}"

			allowAdd = Config.App.Session.checkPermissions if @parentEntity then "#{@parentEntity}.#{@entityName}.add" else "#{@entityName}.add"

			if @readOnly
				allowAdd = false

			data.entityName = @entityName
			data.baseUrl = baseUrl
			data.parentEntity = @parentEntity
			data.parentEntityId = @parentEntityId
			data.allowAdd = allowAdd
			data