define [
	"marionette"
	"config"
], (Marionette, Config)->

	class BaseSingleView extends Marionette.ItemView

		initialize:->
			@readOnly = @getOption("readOnly") ? false
			@entityName = @getOption "entityName"
			@parentEntityId = @getOption "parentEntityId"
			@parentEntity = @getOption "parentEntity"

		serializeData:->
			data = super
			
			baseUrl = ""

			if @parentEntity and @parentEntityId
				baseUrl = "/#{@getOption("parentEntity") ? ""}/#{@getOption("parentEntityId") ? ""}"

			allowEdit = Config.App.Session.checkPermissions if @parentEntity then "#{@parentEntity}.#{@entityName}.edit" else "#{@entityName}.edit"

			if @readOnly
				allowEdit = false

			data.baseUrl = baseUrl
			data.parentEntity = @parentEntity
			data.parentEntityId = @parentEntityId
			data.allowEdit = allowEdit
			data