define [
	"marionette"
	"behaviors/ModelBindBehavior"
	"behaviors/FormBehavior"
], (Marionette)->

	class BaseFormView extends Marionette.ItemView

		attributes:
			class: "form-horizontal cmxform"
			role: "form"

		behaviors:
			ModelBind: {}
			Form: {}