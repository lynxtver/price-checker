define [
	"marionette"
	"config"
], (Marionette, Config)->

	class TreeView extends Marionette.CollectionView

		attributes:
			class: "tree tree-solid-line"