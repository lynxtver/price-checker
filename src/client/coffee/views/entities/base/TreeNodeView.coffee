define [
	"marionette"
	"config"
], (Marionette, Config)->

	class TreeNodeView extends Marionette.CompositeView

		tagName: "div"

		events:
			"click a[data-id]": "onDeleteClick"

		childViewOptions:->
			_.omit @options, "model"

		initialize:->
			@collection = new Backbone.Collection @model.get "children"

		attachHtml:(collectionView, itemView)->
			collectionView.$(".tree-folder-content:first").append itemView.el

		onRender:->
			if @collection.length 
				@$el.addClass "tree-folder"
			else
				@$el.addClass "tree-item"

			entityName = @getOption "entityName"

			unless Config.App.Session.checkPermissions "#{entityName}.edit"
				@$("li.edit").addClass "disabled"
				@$("li.edit a").addClass "disabled"

			unless Config.App.Session.checkPermissions "#{entityName}.remove"
				@$("li.remove").addClass "disabled"
				@$("li.remove a").addClass "disabled"

		onDeleteClick:(e)->
			do e.preventDefault
			id = $(e.currentTarget).data "id"
			if id? and id is @model.id
				self = @
				entityName = @getOption "entityName"
				Config.App.reqres.request "#{entityName}:remove", id
					.done ->
						do self.destroy
			else
				do e.stopPropagation