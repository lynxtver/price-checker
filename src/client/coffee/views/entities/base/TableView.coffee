define [
	"marionette"
	"i18n!locales:datatables,common"
	"config"
	"text!templates/dataTable.html"
	"text!templates/tableActions.html"
	"components/datatables"
], (Marionette, i18n, Config, template, actionsTemplate)->

	initLocale = ->
		$.extend true, $.fn.dataTable.defaults,
			language:
				info: i18n.t "datatables:info.default"
				infoFiltered: i18n.t "datatables:info.filtered"
				infoEmpty: i18n.t "datatables:info.empty"
				emptyTable: i18n.t "datatables:nodata"
				zeroRecords: i18n.t "datatables:info.empty"
				search: i18n.t "datatables:search.label"
				searchPlaceholder: i18n.t "datatables:search.placeholder"
				lengthMenu: i18n.t "datatables:menu"
				thousands: i18n.t "datatables:thousands"
				processing: "<i class=\"fa fa-spinner fa-spin fa-2x\"></i>"
				paginate:
					last: i18n.t "datatables:last"
					next: i18n.t "datatables:next"
					previous: i18n.t "datatables:prev"
					first: i18n.t "datatables:first"


	class DataTableView extends Marionette.ItemView

		@ActionsColumn:
			data: null
			title: "common:actions.title"
			className: "text-center"
			orderable: false
			searchable: false
			actions: true

		attributes:
			class: "adv-table table-advance"

		actionsTemplate: _.template actionsTemplate

		template: _.template template

		ui:
			table: "table"
			header: "thead"
			footer: "tfoot"

		events:
			"click a[data-id]": "onDeleteClick"
			"processing.dt @ui.table": "onProccessing"

		pageLength: 10

		initialize:->
			do initLocale

			@readOnly = @getOption("readOnly") ? false
			@parentEntity = @getOption "parentEntity"
			@parentEntityId = @getOption "parentEntityId"

			@queryData =
				parentEntityId: @parentEntityId
				parentEntity: @parentEntity

			@_showRowNumbers = @getOption "showRowNumbers"
			@_showActions = @getOption "showActions"
			@_columns = _.clone @getOption "columns"

			if @_showRowNumbers
				@_columns.unshift 
					data: null
					title: "datatables:rownumber"
					className: "text-center"
					defaultContent: ""
					orderable: false
					searchable: false
					width: "1%"

			if @_showActions
				@_columns.push
					data: null
					title: "common:actions.title"
					className: "text-center"
					orderable: false
					searchable: false
					width: "1%"
					render: @renderActions

			@listenTo Config.App.vent, "datatable:#{@entityName}:redraw", @onRedraw

		getActionsData: (item)->
			baseUrl = ""

			if @parentEntity and @parentEntityId
				baseUrl = "/#{@parentEntity}/#{@parentEntityId}"

			allowEdit = Config.App.Session.checkPermissions if @parentEntity then "#{@parentEntity}.#{@entityName}.edit" else "#{@entityName}.edit"
			allowRemove = Config.App.Session.checkPermissions if @parentEntity then "#{@parentEntity}.#{@entityName}.remove" else "#{@entityName}.remove"

			if @readOnly
				allowEdit = allowRemove = false

			item: item
			entityName: "#{@entityName}"
			baseUrl: baseUrl
			allowEdit: allowEdit
			allowRemove: allowRemove

		renderActions: (data, type, row, meta)=>
			Marionette.Renderer.render @actionsTemplate, @getActionsData row

		onRender:->

			$headTR = $("<tr>").appendTo @ui.header
			$footerTR = $("<tr>").appendTo @ui.footer

			_.each @_columns, (column, index)->
				title = i18n.t.apply null, if _.isArray column.title then column.title else [column.title]
				if column.rawTitle?
					title = column.rawTitle

				$("<th>")
					.text title
					.appendTo $headTR

				$("<th>")
					.text title
					.appendTo $footerTR
			, @

		onShow:->
			self = @

			ordering = @getOption("ordering") ? true
			url = @getOption "url"
			order = @getOption "order"
			pageLength = @getOption "pageLength"
			queryData = @getOption "queryData"
			createdRow = @getOption "createdRow"
			footerCallback = @getOption "footerCallback"

			columns = _.map @_columns, (column)->
				title = i18n.t.apply null, if _.isArray column.title then column.title else [column.title]
				if column.rawTitle?
					title = column.rawTitle

				_.extend _.omit(column, "title"), 
					title: title

			drawCallback = null

			if @showRowNumbers
				drawCallback = (settings)->
					for i in [0...settings.aiDisplay.length]
						$("td:eq(0)", settings.aoData[settings.aiDisplay[i]].nTr ).html settings._iDisplayStart + i + 1

			@_dataTable = @ui.table.dataTable
				serverSide: true
				processing: true
				searching: true
				ordering: ordering
				lengthChange: true
				createdRow: createdRow
				footerCallback: footerCallback
				pageLength: pageLength
				columns: columns
				order: order
				drawCallback: drawCallback
				ajax: (data, fn, settings)->
					$.ajax
						url: url
						type: "GET"
						dataType: "json"
						data: _.extend {}, data, queryData
						success: fn
						error: self.onAjaxError

			@ui.table.data "view", @

			@

		onAjaxError:=>
			@_dataTable._fnProcessingDisplay false

		onProccessing:(e, settings, processing)=>
			$(settings.aanFeatures.r).css "display", "none"
			if processing
				unless @_proccessing
					@_proccessing = setTimeout ->
						$(settings.aanFeatures.r).css "display", "block"
					, 200
			else
				clearTimeout @_proccessing
				@_proccessing = null
				$(settings.aanFeatures.r).css "display", "none"

		onRedraw:->
			do @_dataTable.api().draw

		onDeleteClick:(e)->
			do e.preventDefault
			id = $(e.currentTarget).data "id"
			if id?
				self = @
				actionName = "#{@entityName}:remove"
				permissionName = "#{@entityName}.remove"
				if self.parentEntity
					actionName = "#{self.parentEntity}:#{actionName}"
					permissionName = "#{self.parentEntity}.#{permissionName}"
				if Config.App.Session.checkPermissions permissionName
					if self.parentEntityId?
						Config.App.reqres.request actionName, self.parentEntityId, id
							.done ->
								self.triggerMethod "redraw"
					else
						Config.App.reqres.request actionName, id
							.done ->
								self.triggerMethod "redraw"