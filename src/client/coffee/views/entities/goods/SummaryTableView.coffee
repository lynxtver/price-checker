define [
	"views/entities/base/TableView"
	"config"
	"components/moment"
	"i18n!locales:datatables,common"
], (TableView, Config, moment, i18n)->

	class SummaryTableView extends TableView

		url: "/api/prices"

		entityName: "prices"

		showRowNumbers: false

		showActions: false

		columns: [
			data: "shop"
			title: "common:entities.shop"
			name: "shop"
			searchable: false
			render: (data, type, row, meta)->
				result = ""
				if data?
					if Config.App.Session.checkPermissions "shops.view"
						result = "<a href=\"/shops/#{data.id}/view\"><i class=\"fa fa-shopping-cart\"></i> #{data.name}</a>"
					else
						result = "<i class=\"fa fa-shopping-cart\"></i> #{data.name}"
				result
		,
			data: "timestamp"
			title: "common:datetime"
			name: "timestamp"
			className: "text-center"
			orderable: false
			searchable: false
			render: (data, type, row, meta)->
				moment(data).format i18n.t "common:locale.datetime"
		,
			data: "prevPrice"
			title: "common:prevPrice"
			name: "prevPrice"
			orderable: false
			searchable: false
			className: "text-center"
			render: (data, type, row, meta)->
				result = ""
				strPrevPrice = ""
				if row? and row.prevPrice?
					prevPrice = row.prevPrice
					strPrevPrice = "<small class=\"text-muted\">#{prevPrice}</small>"
					result = "#{strPrevPrice}"
				result
		,
			data: "price"
			title: "common:entities.price"
			name: "price"
			orderable: true
			searchable: false
			className: "text-center"
			render: (data, type, row, meta)->
				result = ""
				strPrevPrice = ""
				strPrice = ""
				if row? and row.price?
					price = row.price
					prevPrice = row.prevPrice
					strPrice = "<span>#{price}</span>"
					if prevPrice?
						if price > prevPrice
							strPrice = "<span class=\"text-danger\">#{price}</span>"
						else if price < prevPrice
							strPrice = "<span class=\"text-success\">#{price}</span>"
						else
							strPrice = "<span>#{price}</span>"
					if link?
						strPrice = "<a href=\"#{link.url}\" target=\"_blank\">#{strPrice}</a>"
					result = "#{strPrice}"
				result
		]

		order: [0, 'asc']