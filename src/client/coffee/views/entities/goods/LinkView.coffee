define [
	"marionette"
	"text!templates/entities/goods/link.html"
	"components/ShopsInput"
	"behaviors/ModelBindBehavior"	
], (Marionette, template, ShopsInput)->

	class LinkView extends Marionette.ItemView

		attributes:
			class: "form-group"

		template: _.template template

		behaviors:
			ModelBind: {}

		bindings:
			"[name='shop']": 
				observe: "shop"
				setOptions:
					validate: true

			"[name='url']": 
				observe: "url"
				setOptions:
					validate: true

		onShow:->
			@shopInput = new ShopsInput
				el: @$ "[name='shop']"

		onBeforeDestroy:->
			do @shopInput.destroy