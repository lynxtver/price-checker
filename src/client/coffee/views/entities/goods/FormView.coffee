define [
	"views/entities/base/FormView"
	"text!templates/entities/goods/form.html"
	"models/LinkModel"
	"views/entities/goods/LinkView"
	"components/MultiInputView"
], (FormView, template
	LinkModel, LinkView
	MultiInputView)->
	
	class GoodsFormView extends FormView

		template: _.template template

		bindings:
			"#name": 
				observe: "name"
				setOptions:
					validate: true

			"#links": 
				observe: "links"
				setOptions:
					validate: true

		onShow:->

			@components.add new MultiInputView
				el: "#links"
				itemView: LinkView
				itemModel: LinkModel
				addButtonText: "common:addLink"

		onFormDone:->
			Backbone.history.navigate "/goods", true