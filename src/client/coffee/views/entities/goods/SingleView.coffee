define [
	"views/entities/base/SingleView"
	"config"
	"text!templates/entities/goods/single.html"
], (SingleView, Config, template)->

	class GoodView extends SingleView

		template: _.template template

		attributes:
			class: "form-horizontal"

		entityName: "goods"