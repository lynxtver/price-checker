define [
	"components/ControlView"
	"highcharts"
	"async"
	"collections/ShopsCollection"
	"components/moment"
	"i18n!locales:common"
], (ControlView, Highcharts, async, ShopsCollection, moment, i18n) ->

	class GraphView extends ControlView

		attributes:
			class: "main-chart"

		initialize:->
			super

			@shopsCollection = new ShopsCollection

		onShow:->
			self = @

			async.parallel [
				(cb)->
					self.collection.fetch().done ()->
						cb null

				(cb)->
					self.shopsCollection.fetch().done ()->
						cb null
			], (err)->

				data = self.collection.toJSON()

				Highcharts.setOptions
					lang:
						months: i18n.t("common:locale.months").split ","
						shortMonths: i18n.t("common:locale.shortMonths").split ","
						weekdays: i18n.t("common:locale.weekdays").split ","

				_.map data, (item)->
					shop = self.shopsCollection.get item.shop
					if shop? 
						item.name = shop.get "name"
					item

				###
				ykeys = []
				labels = []

				shopLinks = self.model.get "links"

				

				actualShops = _.without actualShops, "date"

				self.shopsCollection.each (shop)->
					return if -1 is _.indexOf actualShops, shop.id
					ykeys.push shop.id
					shopName = shop.get "name"
					shopLink = _.find shopLinks, (item)->shop.id is item?.shop?.id
					if shopLink? and shopLink?.url
						formattedShopName = "<a href=\"#{shopLink.url}\" target=\"_blank\">#{shopName}</a>"
					else
						formattedShopName = "#{shopName}"
					labels.push formattedShopName
				###

				if data.length
					self.$el.highcharts
						title: 
							text: self.model.get "name"
						xAxis:
							type: 'datetime'
						yAxis:
							title:
								text: i18n.t "common:entities.price"
						series: data
				
	GraphView