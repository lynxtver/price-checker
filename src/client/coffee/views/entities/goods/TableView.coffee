define [
	"views/entities/base/TableView"
	"config"
], (TableView, Config)->

	class GoodsTableView extends TableView

		url: "/api/goods"

		entityName: "goods"

		showRowNumbers: true

		showActions: true

		columns: [
			data: "name"
			title: "common:name"
			name: "name"
			searchable: true
		,
			data: null
			title: "common:entities.price"
			name: "lastMinPrice"
			className: "text-right"
			orderable: false
			defaultContent: ""
			render: (data, type, row, meta)->
				result = ""
				price = row.lastMinPrice?.price
				shop = row.lastMinPrice?.shop
				links = row.links
				if shop?
					shopLink = _.find links, (item)->shop.id is item.shop

				if price?
					if shopLink?.url?
						result = "<a href=\"#{shopLink.url}\" target=\"_blank\">#{price}</a>"
					else
						result = "#{price}"
				result
		,
			data: null
			title: "common:entities.shop"
			name: "lastMinPriceShop"
			orderable: false
			defaultContent: ""
			render: (data, type, row, meta)->
				result = ""
				shop = row.lastMinPrice?.shop
				links = row.links
				if shop?
					shopLink = _.find links, (item)->shop.id is item.shop

				if shop? and row.lastMinPrice?.price?
					if shopLink?.url?
						result = "<a href=\"#{shopLink.url}\" target=\"_blank\"><i class=\"fa fa-shopping-cart\"></i> #{shop.name}</a>"
					else
						result = "<i class=\"fa fa-shopping-cart\"></i> #{shop.name}"
				result
		]

		order: [1, 'asc']