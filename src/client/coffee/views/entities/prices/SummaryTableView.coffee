define [
	"views/entities/base/TableView"
	"config"
	"components/moment"
	"i18n!locales:datatables,common"
], (TableView, Config, moment, i18n)->

	class SummaryTableView extends TableView

		url: "/api/prices/summary/goods"

		entityName: "prices"

		showActions: false

		columns: [
			data: "good"
			title: "common:entities.good"
			name: "good"
			orderable: true
			searchable: false
			render: (data, type, row, meta)->
				result = ""
				if data?
					if Config.App.Session.checkPermissions "goods.view"
						result = "<a href=\"/goods/#{data.id}/view\"><i class=\"fa fa-tag\"></i> #{data.name}</a>"
					else
						result = "<i class=\"fa fa-tag\"></i> #{data.name}"
				result
		]

		initialize:->

			shops = @getOption "shops"

			columns = [].concat @columns

			shops.each (shop)=>
				columns.push
					data: shop.id
					rawTitle: shop.get "name"
					name: shop.id
					orderable: false
					searchable: false
					className: "text-center"
					render: (data, type, row, meta)->
						result = ""
						colName = meta.settings.aoColumns[meta.col].name
						item = _.find row.data, (el)->
							el.shop is colName
						link = _.find row.good.links, (el)->
							el.shop is colName
						strPrevPrice = ""
						strPrice = ""
						if item? and item.price?
							price = item.price
							prevPrice = item.prevPrice
							strPrice = "<span>#{price}</span>"
							if prevPrice?
								strPrevPrice = "<small class=\"text-muted\">#{prevPrice}</small><br/>"
								if price > prevPrice
									strPrice = "<span class=\"text-danger\">#{price}</span>"
								else if price < prevPrice
									strPrice = "<span class=\"text-success\">#{price}</span>"
								else
									strPrice = "<span>#{price}</span>"
							if link?
								strPrice = "<a href=\"#{link.url}\" target=\"_blank\">#{strPrice}</a>"
							result = "#{strPrevPrice}#{strPrice}"
						result

			@columns = columns

			super