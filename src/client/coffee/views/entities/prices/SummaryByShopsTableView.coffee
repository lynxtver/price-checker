define [
	"views/entities/base/TableView"
	"config"
	"components/moment"
	"i18n!locales:datatables,common"
], (TableView, Config, moment, i18n)->

	class SummaryByShopsTableView extends TableView

		url: "/api/prices/summary/shops"

		entityName: "prices"

		showActions: false

		columns: [
			data: "shop"
			title: "common:entities.shop"
			name: "shop"
			orderable: true
			searchable: false
			render: (data, type, row, meta)->
				result = ""
				if data?
					if Config.App.Session.checkPermissions "shops.view"
						result = "<a href=\"/shops/#{data.id}/view\"><i class=\"fa fa-tag\"></i> #{data.name}</a>"
					else
						result = "<i class=\"fa fa-tag\"></i> #{data.name}"
				result
		]

		initialize:->

			goods = @getOption "goods"

			columns = [].concat @columns

			goods.each (good)=>
				columns.push
					data: good.id
					rawTitle: good.get "name"
					name: good.id
					orderable: false
					searchable: false
					className: "text-center"
					render: (data, type, row, meta)->
						result = ""
						colName = meta.settings.aoColumns[meta.col].name
						item = _.find row.data, (el)->
							el.good is colName
						good = goods.get colName
						link = _.find good.get("links"), (el)->
							el.shop is row.shop.id
						strPrevPrice = ""
						strPrice = ""
						if item? and item.price?
							price = item.price
							prevPrice = item.prevPrice
							strPrice = "<span>#{price}</span>"
							if prevPrice?
								strPrevPrice = "<small class=\"text-muted\">#{prevPrice}</small><br/>"
								if price > prevPrice
									strPrice = "<span class=\"text-danger\">#{price}</span>"
								else if price < prevPrice
									strPrice = "<span class=\"text-success\">#{price}</span>"
								else
									strPrice = "<span>#{price}</span>"
							if link?
								strPrice = "<a href=\"#{link.url}\" target=\"_blank\">#{strPrice}</a>"
							result = "#{strPrevPrice}#{strPrice}"
						result

			@columns = columns

			super