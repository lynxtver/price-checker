define [
	"views/entities/base/FormView"
	"text!templates/entities/prices/form.html"
	"components/ShopsInput"
	"components/GoodsInput"
	"components/DateTimeInputView"
], (FormView, template, ShopsInput, GoodsInput, DateTimeInputView)->
	
	class PricesFormView extends FormView

		template: _.template template

		bindings:
			"#timestamp":
				observe: "timestamp"
				setOptions:
					validate: true

			"#good": 
				observe: "good"
				setOptions:
					validate: true

			"#shop": 
				observe: "shop"
				setOptions:
					validate: true

			"#price": 
				observe: "price"
				setOptions:
					validate: true

			"#prevPrice": 
				observe: "prevPrice"
				setOptions:
					validate: true

		onFormDone:->
			Backbone.history.navigate "/prices", true

		onShow:->
			@components.add new DateTimeInputView
				el: @$ "#timestamp"

			@components.add new ShopsInput
				el: @$ "#shop"

			@components.add new GoodsInput
				el: @$ "#good"