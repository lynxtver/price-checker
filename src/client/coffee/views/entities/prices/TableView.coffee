define [
	"views/entities/base/TableView"
	"config"
	"components/moment"
	"i18n!locales:datatables,common"
], (TableView, Config, moment, i18n)->

	class PricesTableView extends TableView

		url: "/api/prices"

		entityName: "prices"

		showRowNumbers: true

		showActions: true

		columns: [
			data: "timestamp"
			title: "common:datetime"
			name: "timestamp"
			className: "text-center"
			searchable: false
			render: (data, type, row, meta)->
				moment(data).format i18n.t "common:locale.datetime"
		,
			data: "good"
			title: "common:entities.good"
			name: "good"
			searchable: false
			render: (data, type, row, meta)->
				result = ""
				if data?
					if Config.App.Session.checkPermissions "goods.view"
						result = "<a href=\"/goods/#{data.id}/view\"><i class=\"fa fa-tag\"></i> #{data.name}</a>"
					else
						result = "<i class=\"fa fa-tag\"></i> #{data.name}"
				result
		,
			data: "shop"
			title: "common:entities.shop"
			name: "shop"
			searchable: false
			render: (data, type, row, meta)->
				result = ""
				if data?
					if Config.App.Session.checkPermissions "shops.view"
						result = "<a href=\"/shops/#{data.id}/view\"><i class=\"fa fa-shopping-cart\"></i> #{data.name}</a>"
					else
						result = "<i class=\"fa fa-shopping-cart\"></i> #{data.name}"
				result
		,
			data: "prevPrice"
			title: "common:prevPrice"
			name: "prevPrice"
			defaultContent: ""
			className: "text-right"
			searchable: false
		,
			data: "price"
			title: "common:entities.price"
			name: "price"
			className: "text-right"
			searchable: false
			render: (data, type, row, meta)->
				result = ""
				strPrice = ""
				if data?
					price = data
					prevPrice = row.prevPrice
					strPrice = "<span>#{price}</span>"
					if prevPrice?
						if price > prevPrice
							strPrice = "<span class=\"text-danger\">#{price}</span>"
						else if price < prevPrice
							strPrice = "<span class=\"text-success\">#{price}</span>"
						else
							strPrice = "<span>#{price}</span>"
					result = "#{strPrice}"
				result
		]

		order: [1, 'desc']