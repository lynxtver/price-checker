define [
	"views/entities/base/SingleView"
	"config"
	"text!templates/entities/prices/single.html"
], (SingleView, Config, template)->

	class PriceView extends SingleView

		template: _.template template

		attributes:
			class: "form-horizontal"

		entityName: "prices"