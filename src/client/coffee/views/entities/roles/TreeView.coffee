define [
	"views/entities/base/TreeView"
	"views/entities/roles/TreeNodeView"
], (TreeView, RolesTreeNodeView)->

	class RolesTreeView extends TreeView

		childView: RolesTreeNodeView

		childViewOptions:
			entityName: "roles"