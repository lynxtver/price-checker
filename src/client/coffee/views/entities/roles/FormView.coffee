define [
	"views/entities/base/FormView"
	"text!templates/entities/roles/form.html"
	"components/PermissionsInputView"
	"components/Select2View"
], (FormView, template, PermissionsInputView, Select2View)->
	
	class RolesFormView extends FormView

		template: _.template template

		bindings:
			"#name": 
				observe: "name"
				setOptions:
					validate: true

			"#parentId":
				observe: "parentId"
				setOptions:
					validate: true

			"#permissions":
				observe: "permissions"
				setOptions:
					validate: true

		onShow:->
			@components.add new Select2View
				el: "#parentId"

			@components.add new PermissionsInputView
				el: "#permissions"

		onFormDone:->
			Backbone.history.navigate "/roles", true