define [
	"views/entities/base/SingleView"
	"config"
	"text!templates/entities/roles/single.html"
], (SingleView, Config, template)->

	class RoleView extends SingleView

		template: _.template template

		attributes:
			class: "form-horizontal"

		entityName: "roles"