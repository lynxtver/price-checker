define [
	"views/entities/base/TreeNodeView"
	"config"
	"text!templates/entities/roles/treeNode.html"
], (TreeNodeView, Config, template)->

	class RolesTreeNodeView extends TreeNodeView

		template: _.template template