define [
	"components/ControlView"
	"highcharts"
	"async"
	"collections/GoodsCollection"
	"components/moment"
	"i18n!locales:common"
], (ControlView, Highcharts, async, GoodsCollection, moment, i18n) ->

	class GraphView extends ControlView

		attributes:
			class: "main-chart"

		initialize:->
			super

			@goodsCollection = new GoodsCollection

		onShow:->
			self = @

			async.parallel [
				(cb)->
					self.collection.fetch().done ()->
						cb null

				(cb)->
					self.goodsCollection.fetch().done ()->
						cb null
			], (err)->

				data = self.collection.toJSON()

				data = self.collection.toJSON()

				Highcharts.setOptions
					lang:
						months: i18n.t("common:locale.months").split ","
						shortMonths: i18n.t("common:locale.shortMonths").split ","
						weekdays: i18n.t("common:locale.weekdays").split ","

				_.map data, (item)->
					good = self.goodsCollection.get item.good
					if good? 
						item.name = good.get "name"
					item

				###

				ykeys = []
				labels = []

				actualGoods = _.reduce data, (memo, item)->
					memo = _.union memo, _.keys item
				, []

				actualGoods = _.without actualGoods, "date"

				self.goodsCollection.each (good)->
					return if -1 is _.indexOf actualGoods, good.id
					ykeys.push good.id
					goodName = good.get "name"
					#goodLink = _.find shopLinks, (item)->shop.id is item?.shop?.id
					if goodLink? and goodLink?.url
						formattedGoodName = "<a href=\"#{goodLink.url}\" target=\"_blank\">#{shopName}</a>"
					else
						formattedGoodName = "#{goodName}"
					labels.push formattedGoodName
				###

				if data.length
					self.$el.highcharts
						title: 
							text: self.model.get "name"
						xAxis:
							type: 'datetime'
						yAxis:
							title:
								text: i18n.t "common:entities.price"
						series: data
				
	GraphView