define [
	"views/entities/base/TableView"
], (TableView)->

	class ShopsTableView extends TableView

		url: "/api/shops"

		entityName: "shops"

		showRowNumbers: true

		showActions: true

		columns: [
			data: "name"
			title: "common:name"
			name: "name"
			searchable: true
		]

		order: [1, 'asc']