define [
	"views/entities/base/FormView"
	"text!templates/entities/shops/form.html"
], (FormView, template)->
	
	class ShopsFormView extends FormView

		template: _.template template

		bindings:
			"#name": 
				observe: "name"
				setOptions:
					validate: true

			"#selector": 
				observe: "selector"
				setOptions:
					validate: true

		onFormDone:->
			Backbone.history.navigate "/shops", true