define [
	"views/entities/base/SingleView"
	"config"
	"text!templates/entities/shops/single.html"
], (SingleView, Config, template)->

	class ShopView extends SingleView

		template: _.template template

		attributes:
			class: "form-horizontal"

		entityName: "shops"