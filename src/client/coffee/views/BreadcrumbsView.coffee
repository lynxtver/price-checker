define [
	"marionette"
	"text!templates/breadcrumbs/item.html"
], (Marionette, template)->

	class BreadcrumbsItemView extends Marionette.ItemView

		tagName: "li"

		template: _.template template

		onRender:->
			@$el.addClass "active" if @model.get "active"

	class BreadcrumbsView extends Marionette.CollectionView

		tagName: "ul"

		attributes:
			class: "breadcrumb"

		childView: BreadcrumbsItemView

		initialize:->
			items = @getOption "items", []			
			@collection = new Backbone.Collection items

	BreadcrumbsView