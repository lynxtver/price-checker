define [
	"marionette"
	"i18n!locales"
	"components/moment"
	"text!templates/widgets/clock.html"
], (Marionette, i18n, moment, template)->

	class ClockWidget extends Marionette.ItemView

		attributes:
			class: "profile-nav alt"

		template: _.template template

		ui:
			sec: "li.sec"
			hour: "li.hour"
			min: "li.min"
			time: "p.time"
			monthDay: ".monthDay"
			yearDay: ".yearDay"

		onShow:->
			do @update
			@timeout = setInterval @update, 1000

		onDestroy:->
			clearInterval @timeout

		update:=>
			do @updateSeconds
			do @updateMinutes
			do @updateHours
			do @updateTime
			do @updateDate

		updateTime:->
			@ui.time.text moment().format i18n.t "common:locale.time"

		updateDate:->
			now = moment()
			@ui.monthDay.text now.format i18n.t "common:locale.monthDay"
			@ui.yearDay.text now.format i18n.t "common:locale.yearDay"

		updateSeconds:->
			seconds = new Date().getSeconds()
			degree = seconds * 6
			rotate = "rotate(#{degree}deg)"
			@ui.sec.css
				"transform": rotate
				"-moz-transform": rotate
				"-webkit-transform": rotate

		updateMinutes:->
			mins = new Date().getMinutes()
			degree = mins * 6
			rotate = "rotate(#{degree}deg)"
			@ui.min.css
				"transform": rotate
				"-moz-transform": rotate
				"-webkit-transform": rotate

		updateHours:->
			hours = new Date().getHours()
			mins = new Date().getMinutes()
			degree = hours * 30 + (mins / 2)
			rotate = "rotate(#{degree}deg)"
			@ui.hour.css
				"transform": rotate
				"-moz-transform": rotate
				"-webkit-transform": rotate