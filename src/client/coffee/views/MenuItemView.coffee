define [
	"marionette"
	"text!templates/menu/main/item.html"
	"behaviors/LocaleAwareBehavior"
], (Marionette, template)->

	class MenuItemView extends Marionette.ItemView

		tagName: "li"

		template: _.template template

		ui:
			a: "a"

		behaviors:
			LocaleAware: {}

		onLocaleChange:->
			do @render

		onRender:->
			do @update

		update:->
			if @model.get "active"
				@ui.a.addClass "active"
			else
				@ui.a.removeClass "active"

		setActive:(value)->
			@model.set "active", value
			do @update