define [
	"marionette"
], (Marionette)->

	class TopMenuView extends Marionette.CollectionView

		tagName: "ul"
			
		attributes:
			class: "nav pull-right top-menu"

		getChildView: (item)->
			item.get "view"