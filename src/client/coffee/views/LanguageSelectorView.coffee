define [
	"marionette"
	"config"
	"i18n!locales"
	"text!templates/menu/top/languageSelector/template.html"
	"behaviors/LocaleAwareBehavior"
], (Marionette, Config, i18n, template)->

	class LanguageSelectorView extends Marionette.ItemView

		langs:
			ru: "Русский"
			en: "English"

		tagName: "li"

		attributes:
			class: "dropdown language"

		template: _.template template

		events:
			"click [data-lang]": "onLanguageClick"

		behaviors:
			LocaleAware: {}

		onLocaleChange:->
			do @render

		serializeData:->
			lang = i18n.lng().substring 0, 2
			current:
				lang: lang
				name: @langs[lang]
			langs: @langs

		onLanguageClick:(e)->
			do e.preventDefault
			lng = $(e.currentTarget).data "lang"
			if lng isnt i18n.lng()
				i18n.setLng lng, ->
					if Backbone.history?.fragment?
						Backbone.history.loadUrl Backbone.history.fragment
					Config.App.vent.trigger "locale:change"