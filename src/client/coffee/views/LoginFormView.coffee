define [
	"views/entities/base/FormView"
	"text!templates/login.html"
], (FormView, template)->
	
	class LoginFormView extends FormView

		template: _.template template

		bindings:
			"[name='login']": 
				observe: "login"
				setOptions:
					validate: true
			"[name='password']": 
				observe: "password"
				setOptions:
					validate: true
			"[name='rememberme']":
				observe: "rememberme"

		onFormDone:->
			Backbone.history.navigate "/", true