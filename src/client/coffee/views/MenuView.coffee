define [
	"marionette"
	"text!templates/menu/main/list.html"
	"views/MenuItemView"
], (Marionette, template, MenuItemView)->

	class MenuView extends Marionette.CompositeView

		attributes:
			class: "leftside-navigation"

		template: _.template template

		childViewContainer: "ul"

		childView: MenuItemView

		initialize:->
			@listenTo Backbone.history, "route", @onRoute

		onRoute:(router, route, params)->
			@children.call "setActive", false
			menuId = router.getMenuId?()
			if menuId
				model = @collection.get menuId
				if model?
					view = @children.findByModel model
					if view?
						view.setActive true
